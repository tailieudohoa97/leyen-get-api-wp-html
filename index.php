<?php
    if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start('ob_gzhandler'); else ob_start();
    include("./config.php");

    switch ($page) {
        case 'van':
            // code...
            include("./van.php");
            break;
        case 'tho':
            // code...
            include("./tho.php");
            break;
    	case 'binh':
    		// code...
        	include("./binh.php");
    		break;
    	case 'post-detail':
        	include("./post-detail.php");
    		break;
        case 'gioi-thieu':
            include("./gioi-thieu.php");
            break;
        case 'dieu-khoan-dieu-kien':
            include("./dieu-khoan-dieu-kien.php");
            break;
        case 'chinh-sach-bao-mat':
            include("./chinh-sach-bao-mat.php");
            break;
        case 'search-result':
            include("./filter-results.php");
            break;
        case 'home':
            include("./home.php");
            break;
    	default:
        	include("./404.php");
    		break;
    }
    
?>
