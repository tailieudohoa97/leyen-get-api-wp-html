<script>
Vue.component('navbar',{
    props: ['loaded'],
    template: '<nav class="navbar navbar-expand-lg navbar-dark">\
        <a class="navbar-brand logo" href="/">\
            <img src="/images/logo/logo-1.png" alt="logo" class="logo-img" width="50" height="">\
        </a>\
        <!-- Menu button on mobile -->\
        <button class="navbar-toggler" type="button" data-trigger="#main_nav">Menu</button>\
        <!-- Menu button on mobile.// -->\
        <!-- navbar-collapse -->\
        <div id="main_nav" class="navbar-collapse">\
            <loading-menu v-if="!loaded" class="d-block d-md-none"/>\
            <!-- Menu for both PC and Mobile -->\
            <ul class="navbar-nav nav-right ml-auto">\
                <li class="nav-item <?=$page=='van'?'active':''?>"><a class="nav-link" href="/van/">Văn</a></li>\
                <li class="nav-item <?=$page=='tho'?'active':''?>"><a class="nav-link" href="/tho/">Thơ</a></li>\
                <li class="nav-item <?=$page=='binh'?'active':''?>"><a class="nav-link" href="/binh/">Bình</a></li>\
            </ul>\
            <!-- Menu button on PC -->\
            <button type="button" class="menu-store d-none d-md-block">Menu</button>\
        </div>\
        <!-- navbar-collapse.// -->\
    </nav>\
'})


Vue.component('menu-sidebar',{
    props: ['header_tags', 'header_newPost', 'header_most_viewed', 'loaded'],
    methods: {
        formatDate: (value) => {
            if (value) {
                return moment(String(value)).format('DD/MM/YYYY')
            }
        },
    },
    template: '<div id="menu-sidebar">\
        <loading-menu v-if="!loaded" class="d-none d-md-block"/>\
        <div class="frame-menu-sidebar">\
            <div class="left-menu-sidebar">\
                <div class="module search">\
                    <div class="title-module">Tìm kiếm từ khóa</div>\
                    <input type="search" placeholder="Tìm kiếm từ khóa">\
                </div>\
                <div class="module filter">\
                    <div class="title-module">Lọc theo danh mục</div>\
                    <div class="content-filter">\
                        <span v-for="item in header_tags" :data-id="item.id" :data-target="item.slug">{{ item.name }}</span>\
                    </div>\
                </div>\
                \
                <div class="module socials d-none d-md-block">\
                    <a href="/gioi-thieu/">Giới thiệu</a>\
                    <a href="#">Facebook</a>\
                    <a href="#">Twitter</a>\
                    <a href="#">Instagram</a>\
                </div>\
            </div>\
            <div class="right-menu-sidebar">\
                <div class="title-module">Sắp xếp theo</div>\
                <ul class="nav nav-tabs">\
                    <li><a data-toggle="tab" href="#most-recent" class="active">Bài mới nhất</a></li>\
                    <li><a data-toggle="tab" href="#most-viewed">Bài xem nhiều nhất</a></li>\
                </ul>\
                <div class="tab-content">\
                    <!-- Bài mới nhất -->\
                    <div id="most-recent" class="tab-pane fade in active show">\
                        <a v-for="item in header_newPost" :href="[\'/\'+item.slug+\'.html\']" target="_blank" class="item">\
                            <img v-if="item.link_image" :src="item.link_image" alt="" width="155px" height="auto">\
                            <div class="cont-item">\
                                <p class="cat-item">{{ item.category }}</p>\
                                <p class="name-item" v-html=\'item.title.rendered\'></p>\
                                <p class="author">{{item.author }}</p>\
                                <p class="date">{{ formatDate(item.date) }}</p>\
                            </div>\
                        </a>\
                    </div>\
                    <!-- Bài xem nhiều nhất -->\
                    <div id="most-viewed" class="tab-pane fade in" v-html="header_most_viewed"></div>\
                    <!-- Kết quả bộ lọc -->\
                    <div id="search-widget" class="tab-pane fade in">\
                        <loading-filter />\
                        <div class="search-result"></div>\
                    </div>\
                </div>\
            </div>\
        </div>\
    </div>\
'})

</script>