<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include("./head.html") ?>      
    </head>
    <?php include("./token-headers.php") ?>

    <body class="podcast-page">
        <!-- Header  -->
        <?php include("./header.php")?>
        <!-- End Header  --> 

        <div id="podcast-page" class="scroll-overlay">

            <!-- INTRO PAGE -->
            <section class="intro-page">
                <div class="page-content">
                    <div class="intro-page row">
                        <div class="col-12 col-lg-3 name-page">Podcast</div>
                        <div class="col-12 col-lg-9 cont-intro">
                            <p>Our podcast features exclusive interviews, author-narrated essays, fiction, multipart series, and more. We feature new podcast episodes weekly on Tuesdays.</p>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End - INTRO PAGE -->

            <!-- NEW PODCAST -->
            <section class="new-podcast">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-5 col-xxl-7 new-podcast--media">
                        <div class="new-podcast--img-media">
                            <img src="https://emergencemagazine.org/app/uploads/2022/07/GO_Vagrants_final_WR-PODCAST-1200x1200.jpg" alt="">
                            <div class="new-podcast--button">
                                <p class="play-button button">
                                    <img svg-inline class="icon" src='/images/play-button.svg' alt="play-button" />
                                </p>
                                <p class="pause-button button" style="display: none;">
                                    <img svg-inline class="icon" src='/images/pause-button.svg' alt="pause-button" />
                                </p>                 
                            </div>
                        </div>
                        <div class="new-podcast-media-caption">Illustration by George Outhwaite</div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-7 col-xxl-5 new-podcast--content">
                        <h3 class="new-podcast--subheading d-none">This Week’s Podcast</h3>
                        <h2 class="new-podcast--heading">
                            The Vagrants: Butterfly Land Grabs and Other Climate Migrations
                        </h2>
                        <div class="new-podcast--author">
                            <p>by Cal Flyn</p>
                        </div>
                        <div class="new-podcast--text">
                            <p>In this narrated essay, Cal Flyn observes new species of butterflies arriving in Scotland’s Orkney Islands. As plants and animals migrate northwards on an unprecedented scale, she faces the haunting knowledge that some voices are rising as others fade away.</p>
                        </div>
                        <div class="new-podcast--meta">
                            <div class="new-podcast--button">
                                <p class="play-button button">
                                    <img svg-inline class="icon" src='/images/play-button-mini.svg' alt="play-button-mini" />
                                    Play
                                </p>
                                <p class="pause-button button" style="display: none;">
                                    <img svg-inline class="icon" src='/images/pause-button-mini.svg' alt="pause-button-mini" />
                                    Pause
                                </p>                   
                            </div>       
                            <div class="story-meta">
                                <ul class="story-meta--items">
                                    <li class="story-meta--item">
                                        <span>25 min</span>
                                    </li>
                                    <li class="story-meta--item">
                                        <span>
                                            <time datetime="2022-07-05GMT-070000:00:00" data-timestamp="1657004400" class="has-date-hydrated">2 days ago</time>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                            <div class="only-item-share share-socials">
                                <a class="facebook" href="#" target="_blank" title="Chia sẻ Facebook">
                                    <i class="fa fa-facebook-square" aria-hidden="true"></i> 
                                    <span class="text-share">Chia sẻ lên Facebook</span>
                                </a>
                            </div>
                            <div class="podcast-dropdown social-dropdown d-none" type="button" data-socialdropdown="">
                                <div class="social-dropdown--menu-wrapper">
                                    <ul class="social-dropdown--menu">
                                        <li class="social-dropdown--menu-item">
                                            <a class="social-dropdown--link" href="https://itunes.apple.com/us/podcast/emergence-magazines-podcast/id1368790239" target="_blank" rel="noopener noreferrer">Apple Podcasts</a>
                                        </li>
                                        <li class="social-dropdown--menu-item">
                                            <a class="social-dropdown--link" href="https://podcasts.google.com/feed/aHR0cHM6Ly9lbWVyZ2VuY2VtYWdhemluZS5saWJzeW4uY29tL3Jzcw" target="_blank" rel="noopener noreferrer">Google Podcasts</a>
                                        </li>
                                        <li class="social-dropdown--menu-item">
                                            <a class="social-dropdown--link" href="http://www.stitcher.com/s?fid=181008&amp;refid=stpr" target="_blank" rel="noopener noreferrer">Stitcher</a>
                                        </li>
                                        <li class="social-dropdown--menu-item">
                                            <a class="social-dropdown--link" href="https://open.spotify.com/show/5YpLJHG91UvDVvq8xnajuT?si=tPgFos-tReWHBxX4mgzNPw" target="_blank" rel="noopener noreferrer">Spotify</a>
                                        </li>
                                        <li class="social-dropdown--menu-item">
                                            <a class="social-dropdown--link" href="https://tunein.com/podcasts/Religion--Spirituality-Podcas/Emergence-Magazine-p1118652/" target="_blank" rel="noopener noreferrer">TuneIn</a>
                                        </li>
                                        <button type="button" class="social-dropdown--close" data-close-socialdropdown="" aria-label="Close">
                                            X
                                        </button>
                                    </ul>
                                </div>
                                <button class="social-dropdown--button" aria-label="Subscribe to our podcast">Subscribe to our podcast</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END - NEW PODCAST -->

            <!-- RECENT PODCASTS -->
            <section class="recent-podcasts">
                <div class="page-content">
                    <div class="row">
                        <div class="col-12 col-lg-2 title-sec">
                            <p>Recent podcasts</p>
                        </div>
                        <div class="col-12 col-lg-10 content-sec">
                            <div id="accordion-podcast" class="accordion-default">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                              Collapsible Group Item #1
                                          </button>
                                        </h5>
                                    </div>

                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion-podcast">
                                        <div class="card-body">
                                            <div class="col-6 ml-auto">
                                                <a href="#">
                                                    <div class="name">Daily Edition</div>
                                                    <div class="location">Birmingham, AL</div>
                                                </a>
                                                <a href="#">
                                                    <div class="name">Newsbeat</div>
                                                    <div class="location">Davis, CA</div>
                                                </a>
                                                <a href="#">
                                                    <div class="name">Skylight Books</div>
                                                    <div class="location">Los Angeles, CA</div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                              Collapsible Group Item #2
                                          </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion-podcast">
                                        <div class="card-body">
                                            <div class="col-6 ml-auto">
                                                <a href="#">
                                                    <div class="name">Daily Edition</div>
                                                    <div class="location">Birmingham, AL</div>
                                                </a>
                                                <a href="#">
                                                    <div class="name">Newsbeat</div>
                                                    <div class="location">Davis, CA</div>
                                                </a>
                                                <a href="#">
                                                    <div class="name">Skylight Books</div>
                                                    <div class="location">Los Angeles, CA</div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingThree">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                Collapsible Group Item #3
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion-podcast">
                                        <div class="card-body">
                                            <div class="col-6 ml-auto">
                                                <a href="#">
                                                    <div class="name">Daily Edition</div>
                                                    <div class="location">Birmingham, AL</div>
                                                </a>
                                                <a href="#">
                                                    <div class="name">Newsbeat</div>
                                                    <div class="location">Davis, CA</div>
                                                </a>
                                                <a href="#">
                                                    <div class="name">Skylight Books</div>
                                                    <div class="location">Los Angeles, CA</div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>   
                </div>
            </section>
            <!-- END - RECENT PODCASTS -->

            <!-- PLAYLISTS & SERIES -->
            <section class="playlists-series">
                <div class="page-content">
                    <div class="row">
                        <div class="col-12 title-sec">
                            <p>Playlists & Series</p>
                        </div>
                        <div class="col-12 content-sec">
                            <a href="#" class="playlist">
                                <div class="playlist--img">
                                    <img src="https://emergencemagazine.org/app/uploads/2022/02/001_LP_WR-2200x1604.jpg" alt="">
                                </div>
                                <div class="playlist--header">
                                    <div class="playlist--title">Living with the Unknown: Soundtrack</div>
                                    <div class="playlist--meta">4 episodes</div>
                                </div>
                                <div class="playlist__play mini-play">Open</div>
                            </a>
                            <a href="#" class="playlist">
                                <div class="playlist--img">
                                    <img src="https://emergencemagazine.org/app/uploads/2022/02/001_LP_WR-2200x1604.jpg" alt="">
                                </div>
                                <div class="playlist--header">
                                    <div class="playlist--title">Living with the Unknown: Soundtrack</div>
                                    <div class="playlist--meta">4 episodes</div>
                                </div>
                                <div class="playlist__play mini-play">Open</div>
                            </a>
                            <a href="#" class="playlist">
                                <div class="playlist--img">
                                    <img src="https://emergencemagazine.org/app/uploads/2022/02/001_LP_WR-2200x1604.jpg" alt="">
                                </div>
                                <div class="playlist--header">
                                    <div class="playlist--title">Living with the Unknown: Soundtrack</div>
                                    <div class="playlist--meta">4 episodes</div>
                                </div>
                                <div class="playlist__play mini-play">Open</div>
                            </a>
                            <a href="#" class="playlist">
                                <div class="playlist--img">
                                    <img src="https://emergencemagazine.org/app/uploads/2022/02/001_LP_WR-2200x1604.jpg" alt="">
                                </div>
                                <div class="playlist--header">
                                    <div class="playlist--title">Living with the Unknown: Soundtrack</div>
                                    <div class="playlist--meta">4 episodes</div>
                                </div>
                                <div class="playlist__play mini-play">Open</div>
                            </a>
                            <a href="#" class="playlist">
                                <div class="playlist--img">
                                    <img src="https://emergencemagazine.org/app/uploads/2022/02/001_LP_WR-2200x1604.jpg" alt="">
                                </div>
                                <div class="playlist--header">
                                    <div class="playlist--title">Living with the Unknown: Soundtrack</div>
                                    <div class="playlist--meta">4 episodes</div>
                                </div>
                                <div class="playlist__play mini-play">Open</div>
                            </a>
                            <a href="#" class="playlist">
                                <div class="playlist--img">
                                    <img src="https://emergencemagazine.org/app/uploads/2022/02/001_LP_WR-2200x1604.jpg" alt="">
                                </div>
                                <div class="playlist--header">
                                    <div class="playlist--title">Living with the Unknown: Soundtrack</div>
                                    <div class="playlist--meta">4 episodes</div>
                                </div>
                                <div class="playlist__play mini-play">Open</div>
                            </a>
                            <a href="#" class="playlist">
                                <div class="playlist--img">
                                    <img src="https://emergencemagazine.org/app/uploads/2022/02/001_LP_WR-2200x1604.jpg" alt="">
                                </div>
                                <div class="playlist--header">
                                    <div class="playlist--title">Living with the Unknown: Soundtrack</div>
                                    <div class="playlist--meta">4 episodes</div>
                                </div>
                                <div class="playlist__play mini-play">Open</div>
                            </a>
                            <a href="#" class="playlist">
                                <div class="playlist--img">
                                    <img src="https://emergencemagazine.org/app/uploads/2022/02/001_LP_WR-2200x1604.jpg" alt="">
                                </div>
                                <div class="playlist--header">
                                    <div class="playlist--title">Living with the Unknown: Soundtrack</div>
                                    <div class="playlist--meta">4 episodes</div>
                                </div>
                                <div class="playlist__play mini-play">Open</div>
                            </a>
                        </div>
                        <div class="share-page share-socials">
                            <a class="facebook" href="#" target="_blank" title="Chia sẻ Facebook">
                                <i class="fa fa-facebook-square" aria-hidden="true"></i> 
                                <span class="text-share">Chia sẻ trang lên Facebook</span>
                            </a>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END - PLAYLISTS & SERIES -->

            <!-- BANNER -->
            <section class="banner">
                <img src="https://emergencemagazine.org/app/uploads/2021/04/test.jpg" alt="" width="100%">
            </section>

            <!-- Footer  -->
            <?php include("./footer.html") ?>
            <!-- End Footer  -->
        </div>
    </body>
</html>
<script src="/js/script.js"></script>
<link rel="stylesheet" href="/styles/podcast.css">