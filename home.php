<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("./head.html") ?>
</head>

<body class="home has-cursor">
    <?php include("./loading-page.php")?>
    <!-- Header  -->
    <?php include("./header.php") ?> 
    <!-- End Header  -->

    <div id="audio" style="position: absolute; top: 0; left: 0; visibility: hidden;">
        <audio class="wp-audio-shortcode" id="audio-intro" preload="metadata" style="width: 100%;" controls="controls" src="" data-src="/images/Intro-WindyHill.mp3">
        </audio>
    </div>
    <!-- HOME PAGE -->
    <section class="intro-page">
        <div class="imgs">
            <!-- style="width: 180px; height: 270px;" -->
            <div class="frame-imgs" >
                <div class="frame-img-item pos-1" data-grid-item="">
                    <img class="frame-img-item-img" width="100%" height="100%" src="/images/home-1.jpg"> 
                </div>
                <div class="frame-img-item pos-2" data-grid-item="">
                    <img class="frame-img-item-img" width="100%" height="100%" src="/images/home-2.jpg">        
                </div>
                <div class="frame-img-item pos-3" data-grid-item="">
                    <img class="frame-img-item-img" width="100%" height="100%" src="/images/home-3.jpg">       
                </div>
                <div class="frame-img-item pos-4" data-grid-item="">
                    <img class="frame-img-item-img" width="100%" height="100%" src="/images/home-4.jpg">             
                </div>
                <div class="frame-img-item pos-5" data-grid-item="">
                    <img class="frame-img-item-img" width="100%" height="100%" src="/images/home-5.jpg">             
                </div>
                <div class="frame-img-item pos-6" data-grid-item="">
                    <img class="frame-img-item-img" width="100%" height="100%" src="/images/home-6.jpg">             
                </div>
                <div class="frame-img-item pos-7" data-grid-item="">
                    <img class="frame-img-item-img" width="100%" height="100%" src="/images/home-7.jpg">             
                </div>
                <div class="frame-img-item pos-8" data-grid-item="">
                    <img class="frame-img-item-img" width="100%" height="100%" src="/images/home-8.jpg">             
                </div>
                <div class="frame-img-item pos-9" data-grid-item="">
                    <img class="frame-img-item-img" width="100%" height="100%" src="/images/home-9.jpg">             
                </div>
                <div class="frame-img-item pos-10" data-grid-item="">
                    <img class="frame-img-item-img" width="100%" height="100%" src="/images/home-10.jpg">             
                </div>
            </div>
        </div>
        <h5>Xin chào những tâm hồn đồng điệu</h5>
        <h1>
            Cảm nhận những cung bậc cảm xúc của cuộc sống.!
            <br>
            <i class="fa fa-volume-up" aria-hidden="true" style="display: none;" title="Bật nhạc"></i>
            <i class="fa fa-volume-off" aria-hidden="true" style="display: none;" title="Tắt nhạc">&times</i>
        </h1>
        <div class="home_cursor">
            <div class="home_cursor_content">
                <span>Nhấp để</span>
                <span>Bắt đầu và</span>
                <span>bật Âm thanh</span>
            </div>
        </div>
    </section>
    <a href="<?=$sub??'.'?>/van/" class="skip-intro">Bỏ qua</a>
    <!-- End HOME PAGE -->

    <!-- footer  -->
    <div id="footer">
        <footer-html />
    </div>
    <!-- End footer  -->
</body>
</html>
<?php include("./footer.html") ?> 
<script src="/js/script.js"></script>
<script src="/js/home-page.js"></script>
<link rel="stylesheet" href="/styles/homepage.css">
<script>
    new Vue({
        el: '#footer',
        template: '<footer-html />'
    })
    $(window).load(function(){
        console.log($('#audio audio').data('src')); 
        $('#audio audio').attr('src', $('#audio audio').data('src'));
    });
</script>
