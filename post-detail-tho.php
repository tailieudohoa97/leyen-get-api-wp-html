<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include("./head.html") ?>
    </head>

    <body class="post-detail-tho-page">
        <!-- Header  -->
        <?php include("./header.php")?>
        <!-- End Header  --> 

        <div id="post-detail-tho-page" class="scroll-overlay">    
            <loading-page v-if="!loaded" />

            <section class="detail-tho">
                <div class="detail-content-tho row">                    
                    <div class="col-12 col-lg-6 detail--info">
                        <h1 class="detail--title" v-html='postDetail.title.rendered'></h1>
                        <h4 class="detail--author">{{postDetail.author }}</h4>
                        <div class="detail--audio">
                            <div class="detail--button">
                                <p class="play-button button">
                                    <img svg-inline class="icon" src='/images/play-button.svg' alt="play-button" />
                                </p>
                                <p class="pause-button button" style="display: none;">
                                    <img svg-inline class="icon" src='/images/pause-button.svg' alt="pause-button" />
                                </p>                      
                            </div>
                            <div class="detail--description-button">
                                <p>Nhấn Play để nghe</p>
                                <span><strong class="time"></strong> {{postDetail.author }}</span>
                            </div>
                        </div>
                        <div class="detail--container">
                            <div class="detail--date">{{ formatDate(postDetail.date) }}</div>
                            <div class="detail--content" v-html="replaceText_DetailPost(postDetail.postContent)"></div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 detail--banner">
                        <img class="banner-post" v-if="postDetail.full_image" :src="postDetail.full_image" alt="" width="100%">
                    </div>
                </div>
            </section>

            <section class="detail--note">
                <div class="detail--note-authors row">
                    <div class="col-12 col-lg-7 detail--note-title">
                        <h3>Người đóng góp</h3>
                        <div class="detail--share share-socials">
                            <a class="facebook" :href="['<?=$fb_share_url?>'+postDetail.slug+'.html']" target="_blank" title="Chia sẻ Facebook">
                                <i class="fa fa-facebook-square" aria-hidden="true"></i> 
                                <span class="text-share">Chia sẻ lên Facebook</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-lg-5 detail--note-content" v-html="replaceNguoidonggop(postDetail.acf.nguoi_dong_gop)">
                    </div>
                </div>
            </section>            

            <!-- NEW POST THƠ -->
            <section class="new-post-tho">
                <div class="col-12 col-lg-5 new-post-tho--title ml-auto">
                    <p>Vần thơ từ Yên</p>
                </div>

                <div class="new-post-tho--content">
                    <div id="accordion-detail-tho" class="accordion-default">                            
                        <div v-for="item in newPost" class="card">
                            <div :id="['collapse-'+item.id]" class="collapse col-12 col-lg-7" :aria-labelledby="['heading-'+item.id]" data-parent="#accordion-detail-tho">
                                <div class="card-body">
                                    <div class="detail--info">
                                        <h1 class="detail--title" v-html='item.title.rendered'></h1>
                                        <h4 class="detail--author">{{ item.author }}</h4>
                                        <div class="detail--audio">
                                            <div class="detail--button">
                                                <p class="play-button button">
                                                    <img svg-inline class="icon" src='/images/play-button.svg' alt="play-button" />
                                                </p>
                                                <p class="pause-button button" style="display: none;">
                                                    <img svg-inline class="icon" src='/images/pause-button.svg' alt="pause-button" />
                                                </p>                      
                                            </div>
                                            <div class="detail--description-button">
                                                <p>Nhấn Play để nghe</p>
                                                <span><strong class="time"></strong> {{ item.author }}</span>
                                            </div>
                                        </div>
                                        <div class="detail--container">
                                            <div class="detail--date">{{ formatDate(item.date) }}</div>
                                            <div class="detail--content" v-html="replaceText_DetailPost(item.postContent)"></div>
                                            <div class="detail--read">
                                                <a :href="['/'+item.slug+'.html']" title="Mở liên kết" target="_blank" class="read-detail">Đọc chi tiết</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div :id="['heading-'+item.id]" class="card-header col-12 col-lg-5 ml-auto" >
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" :data-target="['#collapse-'+item.id]" aria-expanded="true" :aria-controls="['collapse-'+item.id]">
                                        <a :href="['/'+item.slug+'.html']" title="Mở liên kết" v-html='item.title.rendered' target="_blank" class="title-post"></a>
                                    </button>
                                </h5>
                            </div>                            
                        </div>
                    </div>
                </div>
            </section>
            <!-- END - NEW POST THƠ  -->


            <!-- Footer  -->
            <footer-html />
            <!-- End Footer  -->
        </div>
    </body>
</html>
<?php include("./footer.html") ?>
<script src="/js/script.js"></script>
<script>
    const loadData_postDetail = '<?php echo addslashes($data1); ?>';

    const vueData_postDetail = JSON.parse(loadData_postDetail);
</script>
<script src="/js/post-detail-tho-script.js"></script>
<link rel="stylesheet" href="/styles/post-detail-tho.css">