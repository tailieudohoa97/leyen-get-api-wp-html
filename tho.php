<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include("./head.html") ?>
        <link rel="stylesheet" href="/styles/tho.css">
    </head>

    <?php 
        // //Lấy thông tin của danh mục Thơ id: 10
        // $data1 = getAPI("https://leyen.life/wp-json/wp/v2/categories/10?_fields=name,description,taxonomy_image"); 

        // //Lấy các bài viết của danh mục Thơ id: 10
        // //full_image, link_image (300 x 300), medium_larger_image (480×270), small_image (150 x 150), postContent, author: biến tự tạo trong function
        // $data2 = getAPI("https://leyen.life/wp-json/wp/v2/posts?_fields=postContent,full_image,id,title,slug,author&categories=10&per_page=5"); 
    ?>
    <body class="tho-page">
        <!-- Header  -->
        <?php include("./header.php")?>
        <!-- End Header  --> 

        <div id="tho-page" class="scroll-overlay">
            <loading-page v-if="!loaded"/>
            <div id="status-background">
                <div class="bg-image"></div>
            </div>
            <!-- INTRO PAGE -->
            <section class="intro-page">
                <div class="intro-page row">
                    <div class="col-12 cont-intro">
                        {{ categoryTho.description }}
                    </div>
                    <div class="col-12 name-page">{{ categoryTho.name }}</div>
                    <div class="col-12 thumbnail-page">
                        <img v-if="categoryTho.taxonomy_image" :src="categoryTho.taxonomy_image" alt="" width="100%">
                    </div>
                </div>
            </section>
            <!-- End - INTRO PAGE -->

            <!-- SHOW BLOG - GRID -->
            <section class="show-blog-grid">
                <div class="blog-grid">          
                    <a v-for="item in categoryTho_postsData" :href="['/'+item.slug+'.html']" target="_blank" class="blog-item">
                        <div class="col-12 blog-item--info">
                            <div class="col-12 blog-item--thumbnail d-none">
                                <img v-if="item.full_image" :src="item.full_image" alt="" width="100%">
                            </div>
                            <div class="blog-item--name" v-html='item.title.rendered'></div>
                            <div class="blog-item--author">
                                {{ item.author }}
                            </div>                            
                        </div>
                        <div class="col-12 blog-item--short-des">
                           <div class="blog-item--short-description" v-html="replaceText(item.postContent)"></div>
                           <div class="blog-item--read">Đọc chi tiết</div>
                        </div>
                    </a>  
                </div>
            </section>
            <!-- END - SHOW BLOG - GRID -->
            <!-- Footer  -->
            <footer-html />
            <!-- End Footer  -->
        </div>
    </body>
</html>
<?php include("./footer.html") ?>
<script src="/js/script.js"></script>
<script> 
    // const vueData_categoryTho = JSON.parse(loadData_categoryTho);
    // const vueData_postTho = JSON.parse(loadData_postTho);

    // console.log("Thông tin Danh mục Thơ:")
    // console.log(vueData_categoryTho);

    // console.log("Data Bài viết - DM Thơ")
    // console.log(vueData_postTho);
</script>
<script src="/js/tho-script.js"></script>