<?php
   function getAPI($url, $method = 'GET'){
      $headers = [
         'Cache-Control: no-cache',
         'Content-Type: application/x-www-form-urlencoded; charset=utf-8',
         'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjEwMDcsIm5hbWUiOiJodWdvIiwiaWF0IjoxNjU3NTMzNjc2LCJleHAiOjE4MTUyMTM2NzZ9.3dxMqQEOmjDE5c8Y3e9lOjAK4MjfEuuezmfTbaV-HjA'
      ];

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL,$url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_USERAGENT , "Get data of ".$url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      $server_output = curl_exec ($ch);
      curl_close ($ch);
      return $server_output;
   }
?>