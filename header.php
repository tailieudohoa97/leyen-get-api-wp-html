<script>
    Vue.component('loading-menu', {
        template: '<div class="loadding-menu" style="display: flex;">\
        <img svg-inline class="icon" src="/images/icon-loading-filter.svg" alt="icon-loading" />\
        </div>',
    })
    Vue.component('loading-page', {
        template: '<div class="loadding-screen" style="display: flex;">\
        <img svg-inline class="icon" src="/images/icon-loading-page.svg" alt="icon-loading-page" />\
        </div>',
    })
    Vue.component('loading-filter', {
        template: '<div class="loadding-filter" style="display: flex;">\
        <img svg-inline class="icon" src="/images/icon-loading-filter.svg" alt="icon-loading-filter" />\
        </div>',
    })
    Vue.mixin({
        data: function() {
            return {
                get fb_share_url() {
                    return "<?php echo $fb_share_url?>";
                }
            }
        },

        methods: {
            replaceNguoidonggop: (value) =>{
                if (value) {
                    // console.log(value); 
                    return value
                    .replace(/tacgia/g,'h4')
                    .replace(/noidung/g,'p')
                }
            },
            replaceText_DetailPost: (value) => {
                if (value) {
                    return value
                    .replace(/\[.+\]/g, '')                 //Bỏ shortcode
                    .replace(/<h1.+<\/h1>/g, '')            //Bỏ thẻ <h1> ở bên trang gốc có
                    .replace(/<h1>[\s\S]*<\/h1>/g, '')      //Bỏ thẻ <h1> ở bên trang gốc có
                    .replace('preload="none"', 'preload="metadata"'); //Thêm lệnh load thời lượng vào audio
                }
            },
            replaceText: (value) => {
                if (value) {
                    return value
                    .replace(/\[.+\]/g, '')             //bỏ shortcode ở bên trang gốc có
                    .replace(/<img.+>/g, '')            //Bỏ hình ảnh ở bên trang gốc có
                    .replace(/<video.+<\/video>/g, '')  //Bỏ Video ở bên trang gốc có

                    .replace(/<h1.+<\/h1>|<span>&#8212;<\/span>/g, '')   //bỏ thẻ <h1>,... ở bên trang gốc có
                    .replace(/<h1>[\s\S]*<\/h1>/g, '')                      //bỏ thẻ <h1> ở bên trang gốc có
                    
                    .replace(/<p><\/p>|<p>\n<\/p>|<p> <\/p>|<p>&#160;<\/p>/g, '') //bỏ các thẻ <p> trống

                    .replace(/<p [^>]*><\/p>/g, '')     //Bỏ thẻ p có style và nội dung trống ở bên trang gốc có
                    .replace('preload="none"', 'preload="metadata"') //Thêm lệnh load thời lượng vào audio 
                    // .replace(/<\/?[^>]+(>|$)/g, "")      //bỏ tag html, cái nào có dạng <abc> hoặc </> là nó xóa
                    .slice(0, 2000);                    //Giới hạn số kí tự
                }
            },
            formatDate: (value) => {
                if (value) {
                    return moment(String(value)).format('DD/MM/YYYY')
                }
            },

            general(){
                //Sài chung trang VĂN - BÌNH
                //LẤY THỜI LƯỢNG AUDIO CHÈN VÀO HTML
                $(".accordion-default .card .card-body .des audio").on('loadeddata', function(i, e){
                    //Làm tròn tới Phút
                    //console.log(Math.round((this.duration ?? 0) / 60)+" phút"); 
                    $(this).parents('.card').find('.card-header button .time').html(Math.round((this.duration ?? 0) / 60)+" phút");  
                });
                //END - LẤY THỜI LƯỢNG AUDIO CHÈN VÀO HTML


                //Sài chung trang VĂN - BÌNH
                // TRIGGER AUDIO/MP3 VĂN - "Đôi bài từ Yên" , BÌNH - "Nghe cùng Yên"
                // Click vào img để PLay audio vừa click và Pause những audio khác đang Play
                $('.card .card-header button .title-post-audio .play-button').click(function(){
                    $('audio').not(this).each(function(){
                        this.pause();
                        $(this).removeClass('fixed-audio');
                        // this.currentTime = 0; //Code reset time
                    })

                    $(this).css("display","none").siblings('.pause-button').css("display","block").parents(".card").find('.collapse audio').addClass('fixed-audio').get(0).play();
                    var h_audio_fixed = $(".accordion-default .card .card-body .des audio").height() + "px";
                    $("footer#footer").css("padding-bottom",h_audio_fixed);
                });

                //Click vào img để Pause audio
                $('.card .card-header button .title-post-audio .pause-button').click(function(){
                    $(this).css("display","none").siblings('.play-button').css("display","block").parents(".card").find('.collapse audio').removeClass('fixed-audio').get(0).pause();
                    $("footer#footer").css("padding-bottom","0");
                });

                //Click vào nút Pause (tức là sẽ đổi thành icon Play) của trình phát
                $('.card .collapse audio').on('pause', function(){
                    $(this).parents(".card ").find('.title-post-audio .play-button').css("display","block").siblings('.pause-button').css("display","none");
                });

                //Click vào nút Play (tức là sẽ đổi thành icon Pause) của trình phát
                $('.card .collapse audio').on('play', function(){
                    $(this).parents(".card ").find('.title-post-audio .play-button').css("display","none").siblings('.pause-button').css("display","block");
                    var h_audio_fixed = $(".accordion-default .card .card-body .des audio").height() + "px";
                    $("footer#footer").css("padding-bottom",h_audio_fixed);
                });
                // END - TRIGGER AUDIO/MP3
            },
        }
    })
</script>
<?php //echo str_replace("\r\n","\\\r\n", file_get_contents("./loading-page.php")) ?>
<?php include('./menu-sidebar.php') ?>
<?php //include("./loading-page.php")?>
<div id="header-vuejs">
<?php /*<!-- Header  -->
    <div class="loadding-screen" style="display: flex;">
      <img svg-inline class="icon" src='/images/icon-loading-page.svg' alt="icon-loading-page" />
    </div>
    <header id="header">
        <menu-sidebar :header_tags="header_tags" :header_newPost="header_newPost" :header_most_viewed="header_most_viewed" />
    </header>
    <header id="sub-header">
        <menu-sidebar :header_tags="header_tags" :header_newPost="header_newPost" :header_most_viewed="header_most_viewed" />
        <div class="progress-container d-none">
            <div class="progress-bar" id="myBar"></div>
        </div>
    </header>
<!-- End - Header  -->
*/?>
</div>

<script src="/js/header-script.js"></script>