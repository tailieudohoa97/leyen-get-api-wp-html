<?php
    include('./token-headers.php');
    
    $action = trim($_REQUEST['action'])??'';
    switch ($action) {
        case 'homepage':
            break;
        case 'header':
            $url = array(
                'data_header' => "https://leyen.life/wp-json/wp/v2/posts?_fields=link_image,category,title,slug,date,author&per_page=12&status=publish",
                'data_tags' => "https://leyen.life/wp-json/wp/v2/categories?include=9,10,300026,300022,300024,300023&_fields=name,id,title,slug&status=publish",
                'data_most_viewed_header' => "https://leyen.life/wp-json/api/v1/popular-post?limit=12",
            );
            break;
        case 'post-detail':
            $url = array(
                'newPost' => "https://leyen.life/wp-json/wp/v2/posts?_fields=postContent,medium_larger_image,link,id,title,slug,date,author&per_page=10&status=publish",
                'newPostTho' => "https://leyen.life/wp-json/wp/v2/posts?_fields=postContent,link,id,title,slug,date,author&categories=10&per_page=15&status=publish",
            );
            break;
        case 'binh':
            $url = array(
                'categoryBinh' => "https://leyen.life/wp-json/wp/v2/categories/300026?_fields=name,description",
                'postBinh' => "https://leyen.life/wp-json/wp/v2/posts?_fields=postContent,medium_big_image,id,title,slug&categories=300026&per_page=6&status=publish",
                'categoryNgheCungYen' => "https://leyen.life/wp-json/wp/v2/categories/300024?_fields=name,description",
                'postNgheCungYen' => "https://leyen.life/wp-json/wp/v2/posts?_fields=postContent,medium_larger_image,link,id,title,slug,date,author&categories=300024&per_page=6&status=publish",
            );
            break;
        case 'tho':
            $url = array(
                'categoryTho' => "https://leyen.life/wp-json/wp/v2/categories/10?_fields=name,description,taxonomy_image",
                'postsTho' => "https://leyen.life/wp-json/wp/v2/posts?_fields=postContent,full_image,id,title,slug,author&categories=10&per_page=5&status=publish",
            );
            break;
        case 'van':
            $url = array(
                'categoryVan' => "https://leyen.life/wp-json/wp/v2/categories/9?_fields=name,description,taxonomy_image,slug",
                'postsVan' => "https://leyen.life/wp-json/wp/v2/posts?_fields=full_image,medium_larger_image,postContent,id,title,slug,link,date,author&categories=9&per_page=7&status=publish",
                'newPost' => "https://leyen.life/wp-json/wp/v2/posts?_fields=link_image,postContent,link,id,title,slug,date,author&per_page=10&status=publish",
            );
            break;
        default:
            $url = false;
            break;
    }
    if($url){
        if(is_array($url))
            foreach ($url as $key => $value){
                $data = getAPI($value);
                $searchData[$key] = json_decode($data, true) ?? $data;
            }
        else
            $searchData = getAPI($url);
        echo json_encode($searchData);
    }
    exit();
?>