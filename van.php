<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include("./head.html") ?>      
    </head>

<body class="van-page">
    <!-- Header  -->
    <?php include("./header.php")?>
    <!-- End Header  --> 
    
    <div id="van-page" class="scroll-overlay">
    </div>
</body>
</html>
<?php include("./footer.html") ?>

<script src="<?=$sub??''?>/js/script.js"></script>
<script src="<?=$sub??''?>/js/van-script.js"></script>
<link rel="stylesheet" href="<?=$sub??''?>/styles/podcast.css">