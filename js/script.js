// --------------------------------
// S C R I P T S  C U S T O M I Z E 
// --------------------------------
// console.log('test chạy code');
$(window).load(function(){
    //Hiệu ứng loading page
    // $('.loadding-screen').remove();
});


$(window).ready(function (){

    // [112204HG] 
    // Di chuyển #menu-sidebar và copy <footer> vào cuối #main_nav
    if($(window).width() <= 767 ){
        $("#menu-sidebar").appendTo("#main_nav");
        $("footer").clone().appendTo("#main_nav");
    };



    // NÚT "MENU" TRÊN MENU MAIN
    $('.header-top nav button').click(function() {
        if ($(this).text().trim() === 'Menu') {
            $('#sub-header').addClass('change-color');
            $('body').addClass('scroll-fixed');
            $('.header-top nav button').text('Đóng');

            setTimeout(function () {
                $('header#header .header-top > #menu-sidebar').animate({width:'toggle'},350);
            }, 500);
        }
        else{
            $('header#header .header-top > #menu-sidebar').animate({width:'toggle'},350);

            setTimeout(function () {
                $('#sub-header').removeClass('change-color');
                $('body').removeClass('scroll-fixed');
                $('.header-top nav button').text('Menu');
            }, 500);
        }
    });
    // END - NÚT "MENU" TRÊN MENU MAIN




   //FIXED SUB-HEADER KHI CUỘN XUỐNG 50PX
   $('body > .scroll-overlay').scroll(function() {
        //Scroll Progress Bar
        var winScroll = $('body > .scroll-overlay').scrollTop();
        var height = $('body > .scroll-overlay')[0].scrollHeight - $('body > .scroll-overlay')[0].clientHeight;
        var scrolled = (winScroll / height) * 100;
        $("#myBar").width(scrolled + "%");
        
        //FIXED SUB-HEADER
        if($('body > .scroll-overlay').scrollTop() > 50){
            $('body').addClass('scroll-fixed'); 
            $('#sub-header').addClass('scroll-fixed-fixed');     
        }
        else{
            $('body').removeClass('scroll-fixed');
            $('#sub-header').removeClass('scroll-fixed-fixed'); 
        }

    }).scroll();
    // END - FIXED SUB-HEADER KHI CUỘN XUỐNG 50PX    
})



// ----------------------------
// S C R I P T S  D E F A U L T
// ----------------------------

//Tự cân bằng chiều cao các khung có liên quan tới Menu header
var h_main_header, h_sidebar_real;

function auto_height(){
    var h_main_header = $('.header-top').height();
    // console.log("CC Menu chính: " + h_main_header + "px");

    var h_sidebar_real = "calc(" + 100 + "vh - " + h_main_header + "px )";
    $('#header #menu-sidebar').css({
        'height': h_sidebar_real,
        'top': h_main_header
    });
    // console.log("CC Menu Sidebar tính được: " + $('#header #menu-sidebar').height() + "px");
    $('#menu-sidebar .tab-content .tab-pane').css('height',h_sidebar_real);    
}

$(window).ready(function() {
    //Kiểm tra kích thước màn hình ngay lần đầu load web
    if($(window).width() >= 768 ){
        setTimeout(function(){
            auto_height();
        },50);
    }
}).on('resize', function() {
    if($(window).width() >= 768 ){
        $('#header #menu-sidebar'
            + ','
            + '#menu-sidebar .tab-content .tab-pane'
            ).css('height','');
        $('#header #menu-sidebar').css('top','');
        
        setTimeout(function(){
            auto_height();
        },50);
    }
});




/// Menu bootstraps offcanvas-active
$(function () {
    'use strict'

    // Open Menu main on Mobile
    $("[data-trigger]").on("click", function(){
        var trigger_id =  $(this).attr('data-trigger');
        $(trigger_id).toggleClass("show");
        $('body').toggleClass("offcanvas-active");
    });

    // Close if press ESC button 
    $(document).on('keydown', function(event) {
        if(event.keyCode === 27) {
            $(".navbar-collapse").removeClass("show");
            $("body").removeClass("overlay-active");
        }
    });

    // Close button - close menu main on mobile 
    $(".btn-close").click(function(e){
        $(".navbar-collapse").removeClass("show");
        $("body").removeClass("offcanvas-active");
    }); 

})


//Effect WOW
// new WOW().init();
