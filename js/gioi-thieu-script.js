// ---------------------
// JS  C U S T O M I Z E 
// ---------------------
var vanPage = new Vue({
    el: '#about-page',
    data: {
        aboutPage: vueData_GioiThieu
    },
    methods: {
        replaceText: (value) => {
            if (value) {
                return value
                .replace(/\[.+\]/g, '')             //bỏ shortcode ở bên trang gốc có
                .replace(/<img.+>/g, '')            //Bỏ hình ảnh ở bên trang gốc có

                .replace(/<p><\/p>|<p>\n<\/p>|<p> <\/p>|<p>&#160;<\/p>/g, '') //bỏ các thẻ <p> trống
            }
        },
        formatDate: (value) => {
            if (value) {
                return moment(String(value)).format('DD/MM/YYYY')
            }
        },
    },
})