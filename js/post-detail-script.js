// ---------------------
// JS  C U S T O M I Z E 
// ---------------------
var detailPost = new Vue({
    el: '#post-detail-page',
    data: {
        postDetail: vueData_postDetail[0],
        newPost: '',
        loaded: false,
    },
    beforeCreate(){
        $.get('/api.php',{ action: 'post-detail' }, function(data) {
            if(data){
                // data = JSON.parse(data);
                this.newPost = data['newPost'];
                // console.log(this.categoryVan_postsData[0]);
            }
            this.loaded = true;
        }.bind(this), 'JSON');
    },
    methods: {
        reroleUpdate(){
            //Code lấy thời lượng của audio/mp3
            $(".detail--post .detail--content audio").on('loadeddata', function(i, e){
                //console.log(Math.round((this.duration ?? 0) / 60)+" phút"); //Làm tròn tới Phút
                // $(this).parents('.card').find('.card-header button .time').text(Math.round((this.duration ?? 0) / 60)+" phút");  
                $(this).parents('.detail--post').find('.detail--description-button strong').html(Math.round((this.duration ?? 0) / 60)+" phút");  
            });


            // Kiểm tra nếu bài viết KHÔNG CÓ Audio thì ẩn nút Play
            if($(".detail--content audio").hasClass("wp-audio-shortcode") == false){
              $(".detail--audio").css("display","none");
              $(".detail--authors").css("margin-bottom","88px");
            }  

            // Ẩn khi không có người đóng góp
            if($('.nguoi_dong_gop').text().trim().length == 0){
                $('.detail--authors').css('visibility','hidden');
            }



            // TRIGGER AUDIO/MP3
            //Click vào img để PLay audio vừa click và Pause những audio khác đang Play
            $('.detail--button .play-button').click(function(){
                $('audio').not(this).each(function(){
                    this.pause();
                    // this.currentTime = 0; //Code reset time
                })
                $(this).css("display","none").siblings('.pause-button').css("display","block").parents(".detail--post").find('audio').addClass('fixed-audio').get(0).play();
                var h_audio_fixed = $(".detail--content audio").height() + "px";
                $("footer#footer").css("padding-bottom",h_audio_fixed);
            });

            //Click vào img để Pause audio
            $('.detail--button .pause-button').click(function(){
                $(this).css("display","none").siblings('.play-button').css("display","block").parents(".detail--post").find('audio').removeClass('fixed-audio').get(0).pause();
                $("footer#footer").css("padding-bottom","0");
            });

            //Click vào nút Pause (tức là sẽ đổi thành icon Play) của trình phát
            $('.detail--post .detail--content audio').on('pause', function(){
                $(this).parents(".detail--post").find('.detail--button .play-button').css("display","block").siblings('.pause-button').css("display","none");
            });

            //Click vào nút Play (tức là sẽ đổi thành icon Pause) của trình phát
            $('.detail--post .detail--content audio').on('play', function(){
                $(this).parents(".detail--post").find('.detail--button .play-button').css("display","none").siblings('.pause-button').css("display","block");
                var h_audio_fixed = $(".detail--content audio").height() + "px";
                $("footer#footer").css("padding-bottom",h_audio_fixed);
            });


            // Initialize Swiper
            $(window).ready(function(){
                new Swiper(".swiper-new-post", {
                    slidesPerView: 5, 
                    spaceBetween: 20,   
                    slidesPerGroup: 1, 
                    // loop: true,  
                    autoplay: {
                        delay: 2500,
                    },
                    loopFillGroupWithBlank: true,
                    pagination: {
                        el: ".swiper-pagination",
                        clickable: true,
                    },
                    navigation: {
                        nextEl: ".swiper-button-next",
                        prevEl: ".swiper-button-prev",
                    },
                    breakpoints: {
                        1367: { //min-width: 1367px
                            slidesPerView: 5,
                            spaceBetween: 20,
                        },
                        1025: { //min-width: 1025px
                            slidesPerView: 4,
                            spaceBetween: 20,
                        },
                        769: { //min-width: 769px
                            slidesPerView: 3,
                            spaceBetween: 20,
                        },
                        480: { //min-width: 480px
                            slidesPerView: 2,
                            spaceBetween: 10,
                        },
                        320: { //min-width: 320px
                            slidesPerView: 1,
                            spaceBetween: 10,
                        },
                    }
                });

                //HOVER SẼ DỪNG CHẠY SLIDER
                $(".swiper").on('mouseover', function() {
                    (this).swiper.autoplay.stop();
                });

                $(".swiper").on('mouseout', function() {
                    (this).swiper.autoplay.start();
                });
            })
        },
    },
    mounted() {
        this.loaded = true;        
    },
    updated(){ //Thay đổi HTML
        this.reroleUpdate();
        this.formatDate();
        this.replaceNguoidonggop();
        this.replaceText_DetailPost();
    },
    template:'<div id="post-detail-page" class="scroll-overlay">\
            <loading-page v-if="!loaded"/>\
\
            <section class="detail--banner">\
                <img class="banner-post" v-if="postDetail.full_image" :src="postDetail.full_image" alt="" width="100%">\
            </section>\
\
            <section class="detail--info">\
                <h1 class="detail--title" v-html=\'postDetail.title.rendered\'></h1>\
                <h4 class="detail--author">{{postDetail.author }}</h4>\
            </section>\
\
            <section class="detail--post">\
                <div class="detail--note">\
                    <div class="detail--audio">\
                        <div class="detail--button">\
                            <p class="play-button button">\
                                <img svg-inline class="icon" src=\'/images/play-button.svg\' alt="play-button" />\
                            </p>\
                            <p class="pause-button button" style="display: none;">\
                                <img svg-inline class="icon" src=\'/images/pause-button.svg\' alt="pause-button" />\
                            </p>                      \
                        </div>\
                        <div class="detail--description-button">\
                            <p>Nhấn Play để nghe</p>\
                            <span><strong class="time"></strong> {{postDetail.author }}</span>\
                        </div>\
                    </div>\
                    <div class="detail--authors">\
                        <div id="detail--author-accordion">                            \
                            <div class="card">\
                                <div id="heading-card" class="card-header" >\
                                    <h5 class="mb-0">\
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-card" aria-expanded="true" aria-controls="collapse-card">\
                                            Người đóng góp\
                                        </button>\
                                    </h5>\
                                </div>\
                                <div id="collapse-card" class="collapse" aria-labelledby="heading-card" data-parent="#detail--author-accordion">\
                                    <div class="card-body">\
                                        <div class="nguoi_dong_gop" v-html="replaceNguoidonggop(postDetail.acf.nguoi_dong_gop)">\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
                \
                <div class="detail--container">\
                    <div class="detail--date">{{ formatDate(postDetail.date) }}</div>\
                    <div class="detail--content" v-html="replaceText_DetailPost(postDetail.postContent)"></div>\
                </div>\
            </section>\
            <div class="detail--share share-socials">\
                <a class="facebook" :href="[fb_share_url+postDetail.slug+\'.html\']" target="_blank" title="Chia sẻ Facebook">\
                    <i class="fa fa-facebook-square" aria-hidden="true"></i> \
                    <span class="text-share">Chia sẻ lên Facebook</span>\
                </a>\
            </div>\
\
            <!-- SLIDER NEW POST -->\
            <section class="slider-new-post">\
                <div class="title">\
                    <h4>Bài viết mới nhất</h4>\
                    <div class="slide-control d-none d-md-flex">\
                        <div class="swiper-button-prev"></div>\
                        <div class="swiper-button-next"></div>\
                    </div>\
                </div>\
                <div class="swiper swiper-new-post">\
                    <div class="swiper-wrapper">\
                        <div v-for="item in newPost" class="swiper-slide">\
                            <div class="card h-100">\
                                <a :href="[\'/\'+item.slug+\'.html\']" target="_blank">\
                                    <img v-if="item.medium_larger_image" :src="item.medium_larger_image" alt="" width="100%">\
                                    <div class="new-post--info">                                    \
                                        <h3 class="new-post--title"  v-html="item.title.rendered"></h3>\
                                        <span class="new-post--author">{{ item.author }}</span>\
                                        <div class="new-post--date">{{ formatDate(item.date) }}</div>\
                                    </div>\
                                </a>\
                            </div>\
                        </div>\
                    </div>\
                    \
                    <div class="swiper-pagination d-flex d-md-none"></div>\
                </div>\
            </section>\
            <!-- END - SHOW BLOG - GRID -->\
\
            <!-- Footer  -->\
            <footer-html />\
            <!-- End Footer  -->\
        </div>\
    '
})