$(window).load(function(){
    //Hiệu ứng loading page
    $('.loadding-screen').remove();
});

$(window).ready(function(){
    // Bật audio
    $('i.fa.fa-volume-up').click(function() {
        $(this).css("display","none").siblings('i.fa.fa-volume-off').css("display","inline-block").parents("body.home").find('audio').get(0).play();
        // console.log('click bật');
    });

    // Tắt audio
    $('i.fa.fa-volume-off').click(function() {
        $(this).css("display","none").siblings('i.fa.fa-volume-up').css("display","inline-block").parents("body.home").find('audio').get(0).pause();
        // console.log('click tắt');
    });
    // Tắt audio
    $('a.skip-intro').click(function() {
        $('#audio audio').get(0).pause();
    });


    //TRỎ CHUỘT BẰNG HTML
    var $cursor = $('.has-cursor .home_cursor');
    const minRange = 10, maxRange = 20;
    var centerPos = {
        x: window.innerWidth/2,
        y: window.innerHeight/2,
    },
    posDiff = { 0: 0, 1: 1, 2: 0, 3: -1, 4: 1, 5: 1, 6: 1, 7: 1, 8: 0, 9: -1 };
    function calcPosDiff(type){
        return type*(Math.random() * (maxRange - minRange) + minRange);
    }
    function calcPosBlur(index, element, pointerPos){
        if(element.mainPos.x != 0 || element.mainPos.y != 0)
            return '';
        return 'blur('+(parseInt(pointerPos)==posDiff[index] ? 0 : Math.abs(pointerPos)*2)+'px)';
    }
    var listCenterImg = $('.frame-img-item').each(function(index, el){
        this.originPos = $(this).offset();
        this.mainPos = {
            x:  centerPos.x - this.originPos.left - this.offsetWidth/2 + calcPosDiff(posDiff[index]),
            y:  centerPos.y - this.originPos.top - this.offsetHeight/2 + calcPosDiff(posDiff[index]),
        };
        // $(el).css({opacity: 1, transform:'translate('+this.mainPos.x+'px,'+this.mainPos.y+'px)'});  
    });
    $(window).load(function(){
        if($cursor){
            //Khi di chuyển trỏ chuột trên trang
            $('.intro-page').on("mousemove", (e)=>{
                if($cursor){
                    $cursor.css({
                        top: (e.pageY - 50) + "px", 
                        left: (e.pageX - 50) + "px",
                    }).addClass("is-active");
                    // console.log($cursor);
                }
            })

            //Khi di chuyển trỏ chuột RA KHỎI trang
            $('.intro-page').on('mouseout', ()=>{
                if($cursor)
                $cursor.removeClass("is-active");
            })

            //Khi click là tắt Hiệu ứng luôn
            $(document).on("click", ()=>{
                if($cursor){
                    $cursor.removeClass("is-active");
                    $cursor = undefined;
                    // console.log('Click là Xóa is-active');
                    $("body.home").removeClass("has-cursor");
                    // console.log('Rồi xóa luôn has-cursor'); 
                    $('.frame-img-item').css({transform: ''});
                    
                    //Bật Audio
                    $('#audio audio').get(0).play();
                    $('i.fa.fa-volume-off').css("display","block");

                    listCenterImg.each(function(index, el){
                        this.mainPos = {
                            x: 0,
                            y: 0,
                        }
                    });
                }
            })        
        }    
    })
    //END - TRỎ CHUỘT BẰNG HTML

    

    var movementDelay = 0;
    $(document).on('mousemove', function(e){
        if(movementDelay < new Date().getTime()){
            e.pageX = e.pageX != undefined ? e.pageX : centerPos.x;
            e.pageY = e.pageY != undefined ? e.pageY : centerPos.y;
            const pointerPos = {
                x: (e.pageX - centerPos.x)/window.innerWidth,
                y: (e.pageY - centerPos.y)/window.innerHeight,
            };
            movementDelay = new Date().getTime() + 50;
            listCenterImg.each(function(index, el){
                const effectPos = {
                    // //Effect di chuyển song song quãng dài
                    // x: (this.mainPos.x + (this.offsetWidth*pointerPos.x)),
                    // y: (this.mainPos.y + (this.offsetHeight*pointerPos.y)),

                    //Effect di chuyển song song quãng ngắn
                    x: (this.mainPos.x + ((this.offsetWidth/2)*pointerPos.x)),
                    y: (this.mainPos.y + ((this.offsetHeight/2)*pointerPos.y)),

                    // //Effect di chuyển lệch tâm ngược quãng dài
                    // x: (this.mainPos.x + (this.mainPos.x * pointerPos.x)),
                    // y: (this.mainPos.y + (this.mainPos.y * pointerPos.y)),
                    
                    // // Effect di chuyển lệch tâm ngược quãng ngắn
                    // x: (this.mainPos.x + (this.mainPos.x/10 * pointerPos.x)),
                    // y: (this.mainPos.y/10 * pointerPos.y)),
                };
                $(el).css({opacity: 1, transform:'translate('+effectPos.x+'px,'+effectPos.y+'px)', filter: calcPosBlur(index, el, (pointerPos.x + pointerPos.y)*1.5)});  
            });
        }
    }).mousemove();
    // listCenterImg.each(function(index, el){ $(el).css({'opacity':1, 'transform': 'translate('+(centerPos.width-pos.x)+'px, '+(centerPos.height-pos.y)+'px)'});
})