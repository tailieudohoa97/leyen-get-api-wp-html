// ---------------------
// JS  C U S T O M I Z E 
// ---------------------
var binhPage = new Vue({
    el: '#binh-page',
    data: {
        categoryBinh: '',
        categoryBinh_postsData: '',
        categoryNgheCungYen: '',
        categoryNgheCungYen_postsData: '',
        loaded: false,
    },
    beforeCreate(){
        $.get('/api.php',{ action: 'binh' }, function(data) {
            if(data){
                // data = JSON.parse(data);
                this.categoryBinh = data['categoryBinh'];
                this.categoryBinh_postsData = data['postBinh'];
                this.categoryNgheCungYen = data['categoryNgheCungYen'];
                this.categoryNgheCungYen_postsData = data['postNgheCungYen'];
            }
            this.loaded = true;
        }.bind(this), 'JSON');
    },
    methods: {
        reroleUpdate(){
        
        },
    },
    updated(){ //Thay đổi HTML
        this.reroleUpdate();
        this.general();
        this.replaceText();
        this.formatDate();
    },
    template: '<div id="binh-page" class="scroll-overlay">\
            <loading-page v-if="!loaded"/>\
            <!-- INTRO PAGE -->\
            <section class="intro-page">\
                <div class="page-content">\
                    <div class="intro-page row">\
                        <div class="col-12 col-lg-3 name-page">{{ categoryBinh.name }}</div>\
                        <div class="col-12 col-lg-9 cont-intro">\
                            {{ categoryBinh.description }}\
                        </div>\
                    </div>\
                    <div class="print-page__store-buttons d-none">\
                        <a class="" href="#" target="_blank">US & Canada Store</a>\
                        <a class="" href="#" target="_blank">International Store</a>\
                    </div>\
                </div>\
            </section>\
            <!-- End - INTRO PAGE -->\
            <!-- SHOW BLOG - GRID -->\
            <section class="show-blog-grid">\
                <div class="page-content">\
                    <div class="blog-grid">                    \
                        <a v-for="item in categoryBinh_postsData" :href="[\'/\'+item.slug+\'.html\']" target="_blank" class="blog-item">\
                            <div class="blog-item--des">\
                                <div class="blog-item--name" v-html=\'item.title.rendered\'></div>\
                                <div class="blog-item--short-description" v-html="replaceText(item.postContent)"></div>\
                            </div>\
                            <div class="blog-item--img">\
                                <!-- <img :src="item[\'_embedded\'][\'wp:featuredmedia\'][0][\'source_url\']" alt="Hình đại diện bài viết"> -->\
                                <img v-if="item.medium_big_image" :src="item.medium_big_image" alt="Hình đại diện bài viết">\
                            </div>\
                        </a>  \
                    </div>\
                </div>\
            </section>\
            <!-- END - SHOW BLOG - GRID -->\
            <!-- NGHE CÙNG YÊN -->\
            <section class="nghe-cung-yen">\
                <div class="page-content">\
                    <div class="row">\
                        <div class="col-12 col-lg-2 title-sec">\
                            <p>{{ categoryNgheCungYen.name }}</p>\
                        </div>\
                        <div class="col-12 col-lg-10 content-sec">\
                            <div id="accordion-binh" class="accordion-default">                            \
                                <div v-for="item in categoryNgheCungYen_postsData" class="card">\
                                    <div :id="[\'heading-\'+item.id]" class="card-header" >\
                                        <h5 class="mb-0">\
                                            <button class="btn btn-link collapsed" data-toggle="collapse" :data-target="[\'#collapse-\'+item.id]" aria-expanded="true" :aria-controls="[\'collapse-\'+item.id]">\
                                                <div class="title-post-audio">\
                                                    <p class="play-button button">\
                                                        <img svg-inline class="icon" src=\'/images/play-button.svg\' alt="play-button" />\
                                                    </p>\
                                                    <p class="pause-button button" style="display: none;">\
                                                        <img svg-inline class="icon" src=\'/images/pause-button.svg\' alt="pause-button" />\
                                                    </p> \
                                                    <div class="name-author-date-time">\
                                                        <a :href="[\'/\'+item.slug+\'.html\']" title="Mở liên kết" v-html=\'item.title.rendered\' target="_blank"></a>\
                                                        <div class="author d-block d-xl-none">\
                                                            {{ item.author }}\
                                                        </div>\
                                                        <div class="date d-block d-xl-none">{{ formatDate(item.date) }}</div>\
                                                        <div class="time d-block d-xl-none"></div>\
                                                    </div>\
                                                </div>\
                                                <div class="author d-none d-xl-block">\
                                                    {{ item.author }}\
                                                </div>\
                                                <div class="date d-none d-xl-block">{{ formatDate(item.date) }}</div>\
                                                <div class="time d-none d-xl-block"></div>\
                                            </button>\
                                        </h5>\
                                    </div>\
                                    <div :id="[\'collapse-\'+item.id]" class="collapse" :aria-labelledby="[\'heading-\'+item.id]" data-parent="#accordion-binh">\
                                        <div class="card-body">\
                                            <div class="row">\
                                                <div class="col-12 col-lg-10 des" v-html="replaceText(item.postContent)"></div>\
                                                <div class="col-12 col-lg-2 img d-none d-lg-block">\
                                                    <img v-if="item.medium_larger_image" :src="item.medium_larger_image" alt="Hình đại diện bài viết" width="100%">\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
            </section>\
            <!-- END - NGHE CÙNG YÊN -->\
            <!-- Footer  -->\
            <footer-html />\
            <!-- End Footer  -->\
        </div>'
})


