// ---------------------
// JS  C U S T O M I Z E 
// ---------------------
var thoPage = new Vue({
    el: '#tho-page',
    data: {
        loaded: false,
        categoryTho: '',
        categoryTho_postsData: ''
    },
    beforeCreate(){
        $.get('/api.php',{ action: 'tho' }, function(data) {
            if(data){
                // data = JSON.parse(data);
                this.categoryTho = data['categoryTho'];
                this.categoryTho_postsData = data['postsTho'];
            }
            this.loaded = true;
        }.bind(this), 'JSON');
    },
    methods: {
        replaceText: (value) => {
            if (value) {
                return value
                .replace(/\[.+\]/g, '')             //bỏ shortcode ở bên trang gốc có
                .replace(/<img.+>/g, '')            //Bỏ hình ảnh ở bên trang gốc có
                .replace(/<video.+<\/video>/g, '')  //Bỏ Video ở bên trang gốc có
                .replace(/<audio.+<\/audio>/g, '')  //Bỏ Audio ở bên trang gốc có
                .replace(/<h1.+<\/h1>|<span>&#8212;<\/span>/g, '')      //bỏ thẻ <h1>,... ở bên trang gốc có
                .replace(/<h1>[\s\S]*<\/h1>/g, '')                      //bỏ thẻ <h1> ở bên trang gốc có

                .replace(/<p><\/p>|<p>\n<\/p>|<p> <\/p>|<p>&#160;<\/p>/g, '') //bỏ các thẻ <p> trống

                .replace(/<p [^>]*><\/p>/g, '')     //Bỏ thẻ p có style và nội dung trống ở bên trang gốc có
                .replace('preload="none"', 'preload="metadata"') //Thêm lệnh load thời lượng vào audio 
                // .replace(/<\/?[^>]+(>|$)/g, "")      //bỏ tag html, cái nào có dạng <abc> hoặc </> là nó xóa
                .slice(0, 2000);                    //Giới hạn số kí tự
            }
        },
        reroleUpdate(){
            // HIỆU ỨNG THAY BACKGROUND TRANG KHI HOVER VÀO TIÊU ĐỀ BÀI VIẾT, TRỞ VỀ BG CŨ KHI CUỘN LÊN TRÊN ĐẦU TRANG
            var countHover = 0;
            // $('.blog-item--info, section.intro-page').mouseenter(function(event) {
            // mouseenter touchstart touchend: click giữ để lướt màn hình
            $('.blog-item--info, section.intro-page').on('mouseenter touchstart touchend',function(event) {
                if(!$(this).hasClass('bgActive')){
                    //add class đảm bảo không bị lỗi đổi background trùng lặp
                    $('.blog-item--info, section.intro-page').removeClass('bgActive');
                    $(this).addClass('bgActive');

                    //Tạo biến lấy src hình bg
                    const bgImg = $(this).find('.blog-item--thumbnail img').attr('src')??'';

                    //Tên Function bên dưới
                    const speed = countHover%2?2000:1500;
                    countHover++; //Thời gian chuyển hiệu ứng
                    fadeBg(bgImg, speed);
                }
            });

            // TRỞ VỀ BG CŨ KHI CUỘN LÊN TRÊN ĐẦU TRANG
            $('body > .scroll-overlay').scroll(function() {
                if($('body > .scroll-overlay').scrollTop() < $('section.intro-page').height() - 200){
                    if(!$('section.intro-page').hasClass('bgActive')){
                        //add class đảm bảo không bị lỗi đổi background trùng lặp
                        $('.blog-item--info, section.intro-page').removeClass('bgActive');
                        $('section.intro-page').addClass('bgActive');

                        //Tạo biến lấy src hình bg
                        const bgImg = $('section.intro-page').find('.blog-item--thumbnail img').attr('src')??'';

                        //Tên Function bên dưới
                        const speed = countHover%2?2000:1500;countHover++;
                        fadeBg(bgImg, speed);
                    }
                }
            }).scroll();
        },
    },
    mounted() {
        
    },
    updated(){ //Thay đổi HTML
        this.reroleUpdate();
        this.formatDate();
    },
    template: '<div id="tho-page" class="scroll-overlay">\
            <loading-page v-if="!loaded"/>\
            <div id="status-background">\
                <div class="bg-image"></div>\
            </div>\
            <!-- INTRO PAGE -->\
            <section class="intro-page">\
                <div class="intro-page row">\
                    <div class="col-12 cont-intro">\
                        {{ categoryTho.description }}\
                    </div>\
                    <div class="col-12 name-page">{{ categoryTho.name }}</div>\
                    <div class="col-12 thumbnail-page">\
                        <img v-if="categoryTho.taxonomy_image" :src="categoryTho.taxonomy_image" alt="" width="100%">\
                    </div>\
                </div>\
            </section>\
            <!-- End - INTRO PAGE -->\
            <!-- SHOW BLOG - GRID -->\
            <section class="show-blog-grid">\
                <div class="blog-grid">\
                    <a v-for="item in categoryTho_postsData" :href="[\'/\'+item.slug+\'.html\']" target="_blank" class="blog-item">\
                        <div class="col-12 blog-item--info">\
                            <div class="col-12 blog-item--thumbnail d-none">\
                                <img v-if="item.full_image" :src="item.full_image" alt="" width="100%">\
                            </div>\
                            <div class="blog-item--name" v-html="item.title.rendered"></div>\
                            <div class="blog-item--author">\
                                {{ item.author }}\
                            </div>                            \
                        </div>\
                        <div class="col-12 blog-item--short-des">\
                           <div class="blog-item--short-description" v-html="replaceText(item.postContent)"></div>\
                           <div class="blog-item--read">Đọc chi tiết</div>\
                        </div>\
                    </a>\
                </div>\
            </section>\
            <!-- END - SHOW BLOG - GRID -->\
            <!-- Footer  -->\
            <footer-html />\
            <!-- End Footer  -->\
        </div>'
})



function fadeBg(bgImg, speed=1000){
    //Chạy animation làm mờ và thay background
    $('#status-background .bg-image').fadeOut(speed, function(){
        $(this).remove();
    });
    $('<div class="bg-image"></div>').css('background-image', 'url(' + bgImg + ')').prependTo($('#status-background'));
}
