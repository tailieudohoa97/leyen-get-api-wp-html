// ---------------------
// JS  C U S T O M I Z E 
// ---------------------

var vueheader = new Vue({
    el: '#header-vuejs',
    data: {
        header_newPost: '',
        header_tags: '',
        header_most_viewed: '',
        loaded: false,
    },
    beforeCreate(){
        $.get('/api.php',{ action: 'header' }, function(data) {
            if(data){
                // data = JSON.parse(data);
                this.header_newPost = data['data_header'];
                this.header_tags = data['data_tags'];
                this.header_most_viewed = data['data_most_viewed_header'];
            }
            this.loaded = true;
        }.bind(this), 'JSON');
    },
    template:'<!-- Header  -->\
<div id="header-vuejs">\
    <header id="header">\
        <div class="header-top">\
            <navbar :loaded="loaded" />\
            <menu-sidebar :header_tags="header_tags" :header_newPost="header_newPost" :header_most_viewed="header_most_viewed" :loaded="loaded" />\
        </div>\
    </header>\
    <header id="sub-header">\
        <div class="header-top">\
            <navbar :header_tags="header_tags" :header_newPost="header_newPost" :header_most_viewed="header_most_viewed" :loaded="loaded" />\
        </div>\
        <div class="progress-container d-none">\
            <div class="progress-bar" id="myBar"></div>\
        </div>\
    </header>\
</div>\
<!-- End - Header  -->'
})


$(window).ready(function (){
    //Tính năng search
    const origin = location.origin;
    var searchRequest = null;
    //Thêm class active khi chọn bộ lọc Danh mục
    $(document).on('click', '.module.filter .content-filter span', function() {
        $(this).toggleClass('active');
        $('.loadding-filter').show();
        var searchInput = $('.module.search input').val().trim();
        var categoryInput = $('.content-filter .active').map((i,v)=>$(v).data('id')).toArray().join(',');
        if(searchInput || categoryInput){
            $('.right-menu-sidebar ul.nav.nav-tabs li').css('visibility','hidden');
            $('#menu-sidebar .tab-content .tab-pane').removeClass("active show");
            $('#menu-sidebar .tab-content #search-widget').addClass("active show");
            $('#menu-sidebar .right-menu-sidebar .title-module').text('Kết quả lọc');
        }else{
            $('.right-menu-sidebar ul.nav.nav-tabs li').css('visibility','initial');
            $('#menu-sidebar .right-menu-sidebar .title-module').text('Sắp xếp theo');
            $('#menu-sidebar .tab-content #search-widget').removeClass("active show");
            $('#menu-sidebar .tab-content .tab-pane:first-child').addClass("active show");
            $('.loadding-filter').hide();
            return;
        }
        if (searchRequest != null) {
            searchRequest.abort();
            searchRequest = null;
        }

        searchRequest = $.post(origin+'/search/', {str_search: searchInput, categories: categoryInput}, function(data, status){
            if(status=='error'){
                $('.search-result').html('<strong>Không tìm thấy bài viết phù hợp!</strong>')
                $('.loadding-filter').hide();
                return;
            }
            $('.search-result').html(data);
            $('.loadding-filter').hide();
        }).fail(function(data, status){
            if(status=='error'){
                $('.search-result').html('<strong>Không tìm thấy bài viết phù hợp!</strong>')
                $('.loadding-filter').hide();
                return;
            }
        })
    });

    $('.module.search input').on('input', function(){
        $('.loadding-filter').show();
        if (searchRequest != null) {
            searchRequest.abort();
            searchRequest = null;
        }
        $('.right-menu-sidebar ul.nav.nav-tabs li').css('visibility','hidden');
        $('#menu-sidebar .tab-content .tab-pane').removeClass("active show");
        $('#menu-sidebar .tab-content #search-widget').addClass("active show");
        $('#menu-sidebar .right-menu-sidebar .title-module').text('Kết quả lọc');
        var searchInput = $(this).val().trim();
        var categoryInput = $('.content-filter .active').map((i,v)=>$(v).data('id')).toArray().join(',');

        if(searchInput == ''){
            $('.right-menu-sidebar ul.nav.nav-tabs li').css('visibility','initial');
            $('#menu-sidebar .right-menu-sidebar .title-module').text('Sắp xếp theo');
            $('#menu-sidebar .tab-content #search-widget').removeClass("active show");
            $('#menu-sidebar .tab-content .tab-pane:first-child').addClass("active show");
            $('.loadding-filter').hide();
            return;
        }

        searchRequest = $.post(origin+'/search/', {str_search: searchInput, categories: categoryInput}, function(data, status){
            if(status!='success'){
                $('.search-result').html('<strong>Không tìm thấy bài viết phù hợp!</strong>')
                $('.loadding-filter').hide();
                return;
            }
            $('.search-result').html(data);
            $('.loadding-filter').hide();
        }).fail(function(data, status){
            if(status=='error'){
                $('.search-result').html('<strong>Không tìm thấy bài viết phù hợp!</strong>')
                $('.loadding-filter').hide();
                return;
            }
        })
    })
})
