// ---------------------
// JS  C U S T O M I Z E 
// ---------------------
var vanPage = new Vue({
    el: '#van-page',
    data: {
        categoryVan: '',
        categoryVan_postsData: '',
        categoryVan_postsAll: '',
        loaded: false,
    },
    beforeCreate(){
        $.get('/api.php',{ action: 'van' }, function(data) {
            if(data){
                // data = JSON.parse(data);
                this.categoryVan = data['categoryVan'];
                this.categoryVan_postsData = data['postsVan'];
                this.categoryVan_postsAll = data['newPost'];
                // console.log(this.categoryVan_postsData[0]);
            }
            this.loaded = true;
        }.bind(this), 'JSON');
    },
    methods: {       
        getAudio: (value) =>{
            if (value) {
                return (value).substring((value).indexOf("<audio"), (value).indexOf("</audio>"))
                .replace('preload="none"', 'preload="metadata"'); 
            }
        },
        reroleUpdate(){
            //Code lấy thời lượng của audio/mp3
            $(".new-podcast--text audio").on('loadeddata', function(i, e){
                $(this).parents('.new-podcast--content').find('.story-meta--items .time').html(Math.round((this.duration ?? 0) / 60)+" phút");  
            });
            
            
            
            $(window).ready(function(){
                $('.wp-video').remove();
            });



            $(window).ready(function() {
                // Kiểm tra nếu bài viết KHÔNG CÓ Audio thì ẩn nút Play 
                if($(".new-podcast--text audio").hasClass("wp-audio-shortcode") == false){
                    $(".new-podcast--button").css("display","none");
                    $(".new-podcast--meta li.story-meta--item.time").css("display","none");
                    $(".new-podcast--meta li.story-meta--item").css("padding","0");
                }     

                // Kiểm tra nếu bài viết KHÔNG CÓ Audio thì ẩn nút Play/Pause chuyển thành nút "Đọc"
                setTimeout(function(){
                    $('.playlist').each(function(){            
                        if($(this).find("audio").hasClass("wp-audio-shortcode") == false){
                            $(this).find(".play-button").css("display","none").siblings('.pause-button').css("display","none").siblings('.detail-button').css("display","flex");
                        }   
                    })
                }, 1000);
            });


            // TRIGGER AUDIO/MP3 bài đầu tiên
            //Click vào img để PLay audio vừa click và Pause những audio khác đang Play
            $('section.new-podcast .new-podcast--button .play-button').click(function(){
                $('audio').not(this).each(function(){
                    this.pause();
                    $(this).removeClass('fixed-audio');
                    // this.currentTime = 0; //Code reset time
                })

                $(this).css("display","none").siblings('.pause-button').css("display","flex").parents("section.new-podcast").find('.new-podcast--text audio').addClass('fixed-audio').get(0).play();
                var h_audio_fixed = $("section.new-podcast .new-podcast--text audio").height() + "px";
                $("footer#footer").css("padding-bottom",h_audio_fixed);
            });

            //Click vào img để Pause audio
            $('section.new-podcast .new-podcast--button .pause-button').click(function(){
                $(this).css("display","none").siblings('.play-button').css("display","flex").parents("section.new-podcast").find('.new-podcast--text audio').removeClass('fixed-audio').get(0).pause();
                $("footer#footer").css("padding-bottom","0");
            });

            //Click vào nút Pause (tức là sẽ đổi thành icon Play) của trình phát
            $('.new-podcast--text audio').on('pause', function(){
                $(this).parents("section.new-podcast").find('.new-podcast--button .play-button').css("display","flex").siblings('.pause-button').css("display","none");
            });

            //Click vào nút Play (tức là sẽ đổi thành icon Pause) của trình phát
            $('.new-podcast--text audio').on('play', function(){
                $(this).parents("section.new-podcast ").find('.new-podcast--button .play-button').css("display","none").siblings('.pause-button').css("display","flex");
                var h_audio_fixed = $("section.new-podcast .new-podcast--text audio").height() + "px";
                $("footer#footer").css("padding-bottom",h_audio_fixed);
            });




            // TRIGGER AUDIO/MP3 Bài viết mới nhất
            //Click vào img để PLay audio vừa click và Pause những audio khác đang Play
            $('.playlist .playlist__play .play-button').click(function(){
                $('audio').not(this).each(function(){
                    this.pause();
                    $(this).removeClass('fixed-audio');
                    // this.currentTime = 0; //Code reset time
                    $(this).parents(".playlist").find('.playlist--description audio').removeClass('fixed-audio');
                })

                $(this).css("display","none").siblings('.pause-button').css("display","flex").parents(".playlist").find('.playlist--description audio').addClass('fixed-audio').get(0).play();
                
                // $('section.new-podcast .new-podcast--button .play-button').css("display","flex").siblings('.pause-button').css("display","none").parents("section.new-podcast").find('.playlist--description audio').removeClass('fixed-audio');
                var h_audio_fixed = $(".playlist .playlist--description audio").height() + "px";
                $("footer#footer").css("padding-bottom",h_audio_fixed);
            });

            //Click vào img để Pause audio
            $('.playlist .playlist__play .pause-button').click(function(){
                $(this).css("display","none").siblings('.play-button').css("display","flex").parents(".playlist").find('.playlist--description audio').removeClass('fixed-audio').get(0).pause();
                $("footer#footer").css("padding-bottom","0");
            });

            //Click vào nút Pause (tức là sẽ đổi thành icon Play) của trình phát
            $('.playlist--description audio').on('pause', function(){
                $(this).parents(".playlist").find('.playlist__play .play-button').css("display","flex").siblings('.pause-button').css("display","none");
            });

            //Click vào nút Play (tức là sẽ đổi thành icon Pause) của trình phát
            $('.playlist--description audio').on('play', function(){
                $(this).parents(".playlist ").find('.playlist__play .play-button').css("display","none").siblings('.pause-button').css("display","flex");
                var h_audio_fixed = $(".playlist .playlist--description audio").height() + "px";
                $("footer#footer").css("padding-bottom",h_audio_fixed);
            });
        },
    },
    mounted() {
           
    },
    updated(){ //Thay đổi HTML
        this.reroleUpdate();
        this.general();
        this.replaceText();
        this.formatDate();
    },
    template: '<div id="van-page" class="scroll-overlay">\
        <loading-page v-if="!loaded"/>\
        <!-- INTRO PAGE -->\
        <section class="intro-page">\
        <div class="page-content">\
        <div class="intro-page row">\
        <div class="col-12 col-lg-3 name-page">{{ categoryVan.name }}</div>\
        <div class="col-12 col-lg-9 cont-intro">\
        {{ categoryVan.description }}\
        </div>\
        </div>\
        </div>\
        </section>\
        <!-- End - INTRO PAGE -->\
        \
        <!-- NEW PODCAST -->\
        <section class="new-podcast" v-if="categoryVan_postsData[0]">\
        <div class="row">\
        <div class="col-12 col-md-6 col-lg-5 col-xxl-7 new-podcast--media">\
        <div class="new-podcast--img-media">\
        <img v-if="categoryVan_postsData[0].full_image" :src="categoryVan_postsData[0].full_image" alt="">\
        <div class="new-podcast--button">\
        <p class="play-button button">\
        <img svg-inline class="icon" src=\'/images/play-button.svg\' alt="play-button" />\
        </p>\
        <p class="pause-button button" style="display: none;">\
        <img svg-inline class="icon" src=\'/images/pause-button.svg\' alt="pause-button" />\
        </p>                        \
        </div>\
        </div>\
        <div class="new-podcast-media-caption d-none">Tác giả hình ảnh</div>\
        </div>\
        <div class="col-12 col-md-6 col-lg-7 col-xxl-5 new-podcast--content">\
        <h3 class="new-podcast--subheading d-none">This Week’s Podcast</h3>\
        <a class="new-podcast--heading" :href="[\'/\'+categoryVan_postsData[0].slug+\'.html\']" v-html=\'categoryVan_postsData[0].title.rendered\' target="_blank"></a>\
        <div class="new-podcast--author">\
        <p>{{ categoryVan_postsData[0].author }}</p>\
        </div>\
        <div class="new-podcast--text">\
        <p v-html="replaceText(categoryVan_postsData[0].postContent)"></p>\
        </div>\
        <div class="new-podcast--meta">\
        <div class="new-podcast--button">\
        <p class="play-button button">\
        <img svg-inline class="icon" src=\'/images/play-button-mini.svg\' alt="play-button-mini" />\
        Play\
        </p>\
        <p class="pause-button button" style="display: none;">\
        <img svg-inline class="icon" src=\'/images/pause-button-mini.svg\' alt="pause-button-mini" />\
        Pause\
        </p>                        \
        </div>       \
        <div class="story-meta">\
        <ul class="story-meta--items">\
        <li class="story-meta--item time">\
        </li>\
        <li class="story-meta--item date">\
        {{ formatDate(categoryVan_postsData[0].date) }}\
        </li>\
        </ul>\
        </div>\
        <div class="only-item-share share-socials">\
        <a class="facebook" :href="[\'<?=$fb_share_url?>\'+categoryVan_postsData[0].slug+\'.html\']" target="_blank" title="Chia sẻ Facebook">\
        <i class="fa fa-facebook-square" aria-hidden="true"></i> \
        <span class="text-share">Chia sẻ lên Facebook</span>\
        </a>\
        </div>\
        <div class="podcast-dropdown social-dropdown d-none" type="button" data-socialdropdown="">\
        <div class="social-dropdown--menu-wrapper">\
        <ul class="social-dropdown--menu">\
        <li class="social-dropdown--menu-item">\
        <a class="social-dropdown--link" href="https://itunes.apple.com/us/podcast/emergence-magazines-podcast/id1368790239" target="_blank" rel="noopener noreferrer">Apple Podcasts</a>\
        </li>\
        <li class="social-dropdown--menu-item">\
        <a class="social-dropdown--link" href="https://podcasts.google.com/feed/aHR0cHM6Ly9lbWVyZ2VuY2VtYWdhemluZS5saWJzeW4uY29tL3Jzcw" target="_blank" rel="noopener noreferrer">Google Podcasts</a>\
        </li>\
        <li class="social-dropdown--menu-item">\
        <a class="social-dropdown--link" href="http://www.stitcher.com/s?fid=181008&amp;refid=stpr" target="_blank" rel="noopener noreferrer">Stitcher</a>\
        </li>\
        <li class="social-dropdown--menu-item">\
        <a class="social-dropdown--link" href="https://open.spotify.com/show/5YpLJHG91UvDVvq8xnajuT?si=tPgFos-tReWHBxX4mgzNPw" target="_blank" rel="noopener noreferrer">Spotify</a>\
        </li>\
        <li class="social-dropdown--menu-item">\
        <a class="social-dropdown--link" href="https://tunein.com/podcasts/Religion--Spirituality-Podcas/Emergence-Magazine-p1118652/" target="_blank" rel="noopener noreferrer">TuneIn</a>\
        </li>\
        <button type="button" class="social-dropdown--close" data-close-socialdropdown="" aria-label="Close">\
        X\
        </button>\
        </ul>\
        </div>\
        <button class="social-dropdown--button" aria-label="Subscribe to our podcast">Subscribe to our podcast</button>\
        </div>\
        </div>\
        </div>\
        </div>\
        </section>\
        <!-- END - NEW PODCAST -->\
        \
        <!-- RECENT PODCASTS -->\
        <section class="recent-podcasts">\
        <div class="page-content">\
        <div class="row">\
        <div class="col-12 col-lg-2 title-sec">\
        <p>Đôi bài từ Yên</p>\
        </div>\
        <div class="col-12 col-lg-10 content-sec">\
        <div id="accordion-van" class="accordion-default">\
        \
        <!-- v-if="index" là nếu thỏa điều kiện index !=0 -->\
        <div v-for="(item, index) in categoryVan_postsData" v-if="index" class="card">\
        <div :id="[\'heading-\'+item.id]" class="card-header" >\
        <h5 class="mb-0">\
        <button class="btn btn-link collapsed" data-toggle="collapse" :data-target="[\'#collapse-\'+item.id]" aria-expanded="true" :aria-controls="[\'collapse-\'+item.id]">\
        <div class="title-post-audio">\
        <p class="play-button button">\
        <img svg-inline class="icon" src=\'/images/play-button.svg\' alt="play-button" />\
        </p>\
        <p class="pause-button button" style="display: none;">\
        <img svg-inline class="icon" src=\'/images/pause-button.svg\' alt="pause-button" />\
        </p> \
        <div class="name-author-date-time">\
        <a :href="[\'/\'+item.slug+\'.html\']" title="Mở liên kết" v-html=\'item.title.rendered\' target="_blank"></a>\
        <div class="author d-block d-xl-none">\
        {{ item.author }}\
        </div>\
        <div class="date d-block d-xl-none">{{ formatDate(item.date) }}</div>\
        <div class="time d-block d-xl-none"></div>\
        </div>\
        </div>\
        <div class="author d-none d-xl-block">\
        {{ item.author }}\
        </div>\
        <div class="date d-none d-xl-block">{{ formatDate(item.date) }}</div>\
        <div class="time d-none d-xl-block"></div>\
        </button>\
        </h5>\
        </div>\
        \
        <div :id="[\'collapse-\'+item.id]" class="collapse" :aria-labelledby="[\'heading-\'+item.id]" data-parent="#accordion-van">\
        <div class="card-body">\
        <div class="row">\
        <div class="col-12 col-lg-10 des" v-html="replaceText(item.postContent)"></div>\
        <div class="col-12 col-lg-2 img d-none d-lg-block">\
        <img v-if="item.medium_larger_image" :src="item.medium_larger_image" alt="" width="100%">\
        </div>\
        </div>\
        </div>\
        </div>\
        </div>\
        </div>\
        </div>\
        </div>   \
        </div>\
        </section>\
        <!-- END - RECENT PODCASTS -->\
        \
        <!-- PLAYLISTS & SERIES -->\
        <section class="playlists-series">\
        <div class="page-content">\
        <div class="row">\
        <div class="col-12 title-sec">\
        <p>Bài viết mới nhất</p>\
        </div>\
        <div class="col-12 content-sec">\
        <div v-for="item in categoryVan_postsAll" class="playlist">\
        <div class="playlist--img">\
        <img v-if="item.link_image" :src="item.link_image" alt="">\
        </div>\
        <div class="playlist--header">\
        <a :href="[\'/\'+item.slug+\'.html\']" title="Mở liên kết" target="_blank" class="playlist--title" v-html=\'item.title.rendered\'></a>\
        <div class="playlist--meta">{{ formatDate(item.date) }}</div>\
        </div>\
        <div class="playlist--description" v-html="getAudio(item.postContent)"></div>\
        <div class="playlist__play mini-play">\
        <p class="play-button button">\
        <img svg-inline class="icon" src=\'/images/play-button-mini.svg\' alt="play-button-mini" />\
        Play\
        </p>\
        <p class="pause-button button" style="display: none;">\
        <img svg-inline class="icon" src=\'/images/pause-button-mini.svg\' alt="pause-button-mini" />\
        Pause\
        </p>     \
        <a :href="[\'/\'+item.slug+\'.html\']" title="Mở liên kết" target="_blank" class="detail-button button" style="display: none;">\
        Đọc\
        </a> \
        </div>                        \
        </div>\
        </div>\
        <div class="share-page share-socials">\
        <a class="facebook" :href="[\'<?=$fb_share_url?>\'+categoryVan.slug+\'/\']" target="_blank" title="Chia sẻ Facebook">\
        <i class="fa fa-facebook-square" aria-hidden="true"></i> \
        <span class="text-share">Chia sẻ trang lên Facebook</span>\
        </a>\
        </div>\
        </div>\
        </div>\
        </section>\
        <!-- END - PLAYLISTS & SERIES -->\
        \
        <!-- BANNER -->\
        <section class="banner">\
        <img v-if="categoryVan.taxonomy_image" :src="categoryVan.taxonomy_image" alt="Hình danh mục" width="100%">\
        </section>\
        \
        <!-- Footer  -->\
        <footer-html />\
        <!-- End Footer  -->\
        </div>\
    '
})

