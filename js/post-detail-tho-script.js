// ---------------------
// JS  C U S T O M I Z E 
// ---------------------
var detailPostTho = new Vue({
    el: '#post-detail-tho-page',
    data: {
        postDetail: vueData_postDetail[0],
        newPost: '',
        loaded: false,
    },
    beforeCreate(){
        $.get('/api.php',{ action: 'post-detail' }, function(data) {
            if(data){
                // data = JSON.parse(data);
                this.newPost = data['newPostTho'];
                // console.log(this.categoryVan_postsData[0]);
            }
            this.loaded = true;
        }.bind(this), 'JSON');
    },
    methods: {
        reroleUpdate(){
            //Code lấy thời lượng của audio/mp3
            $(".detail--post .detail--content audio").on('loadeddata', function(i, e){
                //console.log(Math.round((this.duration ?? 0) / 60)+" phút"); //Làm tròn tới Phút
                // $(this).parents('.card').find('.card-header button .time').text(Math.round((this.duration ?? 0) / 60)+" phút");  
                $(this).parents('.detail--post').find('.detail--description-button strong').html(Math.round((this.duration ?? 0) / 60)+" phút");  
            });


            // HOVER = CLICK MỤC VẦN THƠ TỪ YÊN
            $('.card .card-header button').on("mouseenter",function(){
                $(this).parents('.card').find('[id*="collapse-"]').css('display','block');
            });
            $('.card .card-header button').on("mouseleave",function(){
                $(this).parents('.card').find('[id*="collapse-"]').css('display','none');
            });


            // Kiểm tra nếu bài viết KHÔNG CÓ Audio thì ẩn nút Play
            $(".detail--content").each(function(){
                if($(this).find("audio").hasClass("wp-audio-shortcode") == false){
                  $(this).parents('.detail--info').find(".detail--audio").css("display","none");
                  $(this).parents('.detail--info').find(".detail--container").css("margin-top","55px");
                }    
            });


            // TRIGGER AUDIO/MP3 bài đầu tiên
            //Click vào img để PLay audio vừa click và Pause những audio khác đang Play
            $('section.detail-tho .detail--button .play-button').click(function(){
                $('audio').not(this).each(function(){
                    this.pause();
                    $(this).removeClass('fixed-audio');
                    // this.currentTime = 0; //Code reset time
                })
                $(this).css("display","none").siblings('.pause-button').css("display","block").parents(".detail-content-tho").find('audio').addClass('fixed-audio').get(0).play();
                var h_audio_fixed = $(".detail--content audio").height() + "px";
                $("footer#footer").css("padding-bottom",h_audio_fixed);
            });

            //Click vào img để Pause audio
            $('section.detail-tho .detail--button .pause-button').click(function(){
                $(this).css("display","none").siblings('.play-button').css("display","block").parents(".detail-content-tho").find('audio').removeClass('fixed-audio').get(0).pause();
                $("footer#footer").css("padding-bottom","0");
            });

            //Click vào nút Pause (tức là sẽ đổi thành icon Play) của trình phát
            $('.detail-content-tho .detail--content audio').on('pause', function(){
                $(this).parents(".detail-content-tho").find('.detail--button .play-button').css("display","block").siblings('.pause-button').css("display","none");
            });

            //Click vào nút Play (tức là sẽ đổi thành icon Pause) của trình phát
            $('.detail-content-tho .detail--content audio').on('play', function(){
                $(this).parents(".detail-content-tho").find('.detail--button .play-button').css("display","none").siblings('.pause-button').css("display","block");
                var h_audio_fixed = $(".detail--content audio").height() + "px";
                $("footer#footer").css("padding-bottom",h_audio_fixed);
            });




            // TRIGGER AUDIO/MP3 Bài viết mới nhất
            //Click vào img để PLay audio vừa click và Pause những audio khác đang Play
            $('.card .detail--button .play-button').click(function(){
                $('audio').not(this).each(function(){
                    this.pause();
                    $(this).removeClass('fixed-audio');
                    // this.currentTime = 0; //Code reset time
                })
                $(this).css("display","none").siblings('.pause-button').css("display","block").parents(".card").find('audio').addClass('fixed-audio').get(0).play();
                var h_audio_fixed = $(".detail--content audio").height() + "px";
                $("footer#footer").css("padding-bottom",h_audio_fixed);
            });

            //Click vào img để Pause audio
            $('.card .detail--button .pause-button').click(function(){
                $(this).css("display","none").siblings('.play-button').css("display","block").parents(".card").find('audio').removeClass('fixed-audio').get(0).pause();
                $("footer#footer").css("padding-bottom","0");
            });

            //Click vào nút Pause (tức là sẽ đổi thành icon Play) của trình phát
            $('.card .detail--content audio').on('pause', function(){
                $(this).parents(".card").find('.detail--button .play-button').css("display","block").siblings('.pause-button').css("display","none");
            });

            //Click vào nút Play (tức là sẽ đổi thành icon Pause) của trình phát
            $('.card .detail--content audio').on('play', function(){
                $(this).parents(".card").find('.detail--button .play-button').css("display","none").siblings('.pause-button').css("display","block");
                var h_audio_fixed = $(".detail--content audio").height() + "px";
                $("footer#footer").css("padding-bottom",h_audio_fixed);
            });
        },        
    },
    mounted() {
        this.loaded = true;
    },
    updated(){ //Thay đổi HTML
        this.reroleUpdate();
        this.formatDate();
        this.replaceNguoidonggop();
        this.replaceText_DetailPost();
    },
    template: '<div id="post-detail-tho-page" class="scroll-overlay">\
            <loading-page v-if="!loaded" />\
            <section class="detail-tho">\
                <div class="detail-content-tho row">                    \
                    <div class="col-12 col-lg-6 detail--info">\
                        <h1 class="detail--title" v-html="postDetail.title.rendered"></h1>\
                        <h4 class="detail--author">{{postDetail.author }}</h4>\
                        <div class="detail--audio">\
                            <div class="detail--button">\
                                <p class="play-button button">\
                                    <img svg-inline class="icon" src="/images/play-button.svg" alt="play-button" />\
                                </p>\
                                <p class="pause-button button" style="display: none;">\
                                    <img svg-inline class="icon" src="/images/pause-button.svg" alt="pause-button" />\
                                </p>                      \
                            </div>\
                            <div class="detail--description-button">\
                                <p>Nhấn Play để nghe</p>\
                                <span><strong class="time"></strong> {{postDetail.author }}</span>\
                            </div>\
                        </div>\
                        <div class="detail--container">\
                            <div class="detail--date">{{ formatDate(postDetail.date) }}</div>\
                            <div class="detail--content" v-html="replaceText_DetailPost(postDetail.postContent)"></div>\
                        </div>\
                    </div>\
                    <div class="col-12 col-lg-6 detail--banner">\
                        <img class="banner-post" v-if="postDetail.full_image" :src="postDetail.full_image" alt="" width="100%">\
                    </div>\
                </div>\
            </section>\
\
            <section class="detail--note">\
                <div class="detail--note-authors row">\
                    <div class="col-12 col-lg-7 detail--note-title">\
                        <h3>Người đóng góp</h3>\
                        <div class="detail--share share-socials">\
                            <a class="facebook" :href="[fb_share_url+postDetail.slug+\'.html\']" target="_blank" title="Chia sẻ Facebook">\
                                <i class="fa fa-facebook-square" aria-hidden="true"></i> \
                                <span class="text-share">Chia sẻ lên Facebook</span>\
                            </a>\
                        </div>\
                    </div>\
                    <div class="col-12 col-lg-5 detail--note-content" v-html="replaceNguoidonggop(postDetail.acf.nguoi_dong_gop)">\
                    </div>\
                </div>\
            </section>            \
\
            <!-- NEW POST THƠ -->\
            <section class="new-post-tho">\
                <div class="col-12 col-lg-5 new-post-tho--title ml-auto">\
                    <p>Vần thơ từ Yên</p>\
                </div>\
\
                <div class="new-post-tho--content">\
                    <div id="accordion-detail-tho" class="accordion-default">                            \
                        <div v-for="item in newPost" class="card">\
                            <div :id="[\'collapse-\'+item.id]" class="collapse col-12 col-lg-7" :aria-labelledby="[\'heading-\'+item.id]" data-parent="#accordion-detail-tho">\
                                <div class="card-body">\
                                    <div class="detail--info">\
                                        <h1 class="detail--title" v-html=\'item.title.rendered\'></h1>\
                                        <h4 class="detail--author">{{ item.author }}</h4>\
                                        <div class="detail--audio">\
                                            <div class="detail--button">\
                                                <p class="play-button button">\
                                                    <img svg-inline class="icon" src=\'/images/play-button.svg\' alt="play-button" />\
                                                </p>\
                                                <p class="pause-button button" style="display: none;">\
                                                    <img svg-inline class="icon" src=\'/images/pause-button.svg\' alt="pause-button" />\
                                                </p>                      \
                                            </div>\
                                            <div class="detail--description-button">\
                                                <p>Nhấn Play để nghe</p>\
                                                <span><strong class="time"></strong> {{ item.author }}</span>\
                                            </div>\
                                        </div>\
                                        <div class="detail--container">\
                                            <div class="detail--date">{{ formatDate(item.date) }}</div>\
                                            <div class="detail--content" v-html="replaceText_DetailPost(item.postContent)"></div>\
                                            <div class="detail--read">\
                                                <a :href="[\'/\'+item.slug+\'.html\']" title="Mở liên kết" target="_blank" class="read-detail">Đọc chi tiết</a>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
\
                            <div :id="[\'heading-\'+item.id]" class="card-header col-12 col-lg-5 ml-auto" >\
                                <h5 class="mb-0">\
                                    <button class="btn btn-link collapsed" data-toggle="collapse" :data-target="[\'#collapse-\'+item.id]" aria-expanded="true" :aria-controls="[\'collapse-\'+item.id]">\
                                        <a :href="[\'/\'+item.slug+\'.html\']" title="Mở liên kết" v-html="item.title.rendered" target="_blank" class="title-post"></a>\
                                    </button>\
                                </h5>\
                            </div>                            \
                        </div>\
                    </div>\
                </div>\
            </section>\
            <!-- END - NEW POST THƠ  -->\
\
            <!-- Footer  -->\
            <footer-html />\
            <!-- End Footer  -->\
            </div>\
    '
})