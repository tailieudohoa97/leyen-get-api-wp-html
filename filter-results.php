<?php
    $str_search = trim($_REQUEST['str_search']??'')?'&search='.trim($_REQUEST['str_search']):'';
    $categories = trim($_REQUEST['categories']??'')?'&categories='.trim($_REQUEST['categories']):'';
    if($str_search == '' && $categories==''){
        echo 'No input found!';
        http_response_code(404);
        exit;
    }
    //Lấy data search
    $searchData = getAPI("https://leyen.life/wp-json/wp/v2/posts?_fields=title,slug,date,link_image,author,category$str_search$categories");
    if($searchData == '[]'){
        echo 'No data found!';
        http_response_code(404);
        exit;
    }
    $searchData = json_decode($searchData, true);
?>
<?php foreach ($searchData as $post): ?>
<a href="/<?=$post['slug']?>.html" target="_blank" class="item">
    <img src="<?=$post['link_image']?>" alt="">
    <div class="cont-item">
        <p class="cat-item"><?=$post['category']?></p>
        <p class="name-item"><?=$post['title']['rendered']?></p>
        <p class="author"><?=$post['author']?></p>
        <p class="date"><?=date('d-m-Y',strtotime($post['date']));?></p>
    </div>
</a>
<?php endforeach; ?>
<!-- <a v-for="item in header_newPost" :href="['/'+item.slug+'.html']" target="_blank" class="item">
    <img :src="item._embedded['wp:featuredmedia'][0].source_url" alt="">
    <div class="cont-item">
        <p class="cat-item">{{ item._embedded['wp:term'][0][0].name }}</p>
        <p class="name-item" v-html='item.title.rendered'></p>
        <p class="author">{{item._embedded['author'][0].name }}</p>
        <p class="date">{{ formatDate(item.date) }}</p>
    </div>
</a> -->