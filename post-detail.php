<?php $post_slug = $_REQUEST['slug']??'';?>
<?php 
    //Lấy slug tạo Post Detail   - slug cụ thể mẫu: nang-nhat-leyen, co-phai-thu-phai
    //full_image, link_image (300 x 300), medium_larger_image (480×270), small_image (150 x 150), postContent, author: biến tự tạo trong function
    $data1 = getAPI("https://leyen.life/wp-json/wp/v2/posts?slug=$post_slug&_fields=full_image,postContent,acf,categories,link,id,title,slug,date,author"); 

    //Lấy các bài viết không phân biệt danh mục làm Bài viết mới nhất - show 10 bài
    //$data2 = getAPI("https://leyen.life/wp-json/wp/v2/posts?_fields=postContent,medium_larger_image,link,id,title,slug,date,author&per_page=10");

    //Lấy các bài viết thuộc danh mục THƠ id=10 làm Bài viết mới nhất - show 10 bài
    //$data3 = getAPI("https://leyen.life/wp-json/wp/v2/posts?_fields=postContent,link,id,title,slug,date,author&categories=10&per_page=15");


    //Không có data thì chuyển layout 404
    if($data1 == '[]'){
        header('HTTP/1.1 404 Not Found');
        // Chuyển link thành ...404/ luôn thì dùng dòng dưới 
        // header('Location: '.'http://' . $_SERVER['HTTP_HOST'].'/404/') 
        include '404.php';
        return;
    }

    //Nếu bài viết thuộc Danh mục Thơ id=10 thì chuyển layout Chi tiết bài viết được thiết kế riêng cho Thơ
    $postDetail = json_decode($data1, true);
    if(in_array(10, $postDetail[0]['categories'])){
        include 'post-detail-tho.php';
        return;
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include("./head.html") ?>
    </head>

    <body class="post-detail-page">
        <!-- Header  -->
        <?php include("./header.php")?>
        <!-- End Header  --> 

        <div id="post-detail-page" class="scroll-overlay">    
            <loading-page v-if="!loaded"/>

            <section class="detail--banner">
                <img class="banner-post" v-if="postDetail.full_image" :src="postDetail.full_image" alt="" width="100%">
            </section>

            <section class="detail--info">
                <h1 class="detail--title" v-html='postDetail.title.rendered'></h1>
                <h4 class="detail--author">{{postDetail.author }}</h4>
            </section>

            <section class="detail--post">
                <div class="detail--note">
                    <div class="detail--audio">
                        <div class="detail--button">
                            <p class="play-button button">
                                <img svg-inline class="icon" src='/images/play-button.svg' alt="play-button" />
                            </p>
                            <p class="pause-button button" style="display: none;">
                                <img svg-inline class="icon" src='/images/pause-button.svg' alt="pause-button" />
                            </p>                      
                        </div>
                        <div class="detail--description-button">
                            <p>Nhấn Play để nghe</p>
                            <span><strong class="time"></strong> {{postDetail.author }}</span>
                        </div>
                    </div>
                    <div class="detail--authors">
                        <div id="detail--author-accordion">                            
                            <div class="card">
                                <div id="heading-card" class="card-header" >
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse-card" aria-expanded="true" aria-controls="collapse-card">
                                            Người đóng góp
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapse-card" class="collapse" aria-labelledby="heading-card" data-parent="#detail--author-accordion">
                                    <div class="card-body">
                                        <div class="nguoi_dong_gop" v-html="replaceNguoidonggop(postDetail.acf.nguoi_dong_gop)">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="detail--container">
                    <div class="detail--date">{{ formatDate(postDetail.date) }}</div>
                    <div class="detail--content" v-html="replaceText_DetailPost(postDetail.postContent)"></div>
                </div>
            </section>
            <div class="detail--share share-socials">
                <a class="facebook" :href="['<?=$fb_share_url?>'+postDetail.slug+'.html']" target="_blank" title="Chia sẻ Facebook">
                    <i class="fa fa-facebook-square" aria-hidden="true"></i> 
                    <span class="text-share">Chia sẻ lên Facebook</span>
                </a>
            </div>

            <!-- SLIDER NEW POST -->
            <section class="slider-new-post">
                <div class="title">
                    <h4>Bài viết mới nhất</h4>
                    <div class="slide-control d-none d-md-flex">
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
                <div class="swiper swiper-new-post">
                    <div class="swiper-wrapper">
                        <div v-for="item in newPost" class="swiper-slide">
                            <div class="card h-100">
                                <a :href="['/'+item.slug+'.html']" target="_blank">
                                    <img v-if="item.medium_larger_image" :src="item.medium_larger_image" alt="" width="100%">
                                    <div class="new-post--info">                                    
                                        <h3 class="new-post--title"  v-html="item.title.rendered"></h3>
                                        <span class="new-post--author">{{ item.author }}</span>
                                        <div class="new-post--date">{{ formatDate(item.date) }}</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="swiper-pagination d-flex d-md-none"></div>
                </div>
            </section>
            <!-- END - SHOW BLOG - GRID -->


            <!-- Footer  -->
            <footer-html />
            <!-- End Footer  -->
        </div>
    </body>
</html>
<?php include("./footer.html") ?>
<script src="/js/script.js"></script>
<script> 
    const loadData_postDetail = '<?php echo addslashes($data1); ?>';

    const vueData_postDetail = JSON.parse(loadData_postDetail);
</script>
<script src="/js/post-detail-script.js"></script>
<link rel="stylesheet" href="/styles/post-detail.css">