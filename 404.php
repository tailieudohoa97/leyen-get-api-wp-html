<!DOCTYPE html>
<html lang="en">
<head> <?php include("./head.html") ?> </head>
<body class="page-404">
    <div id="page-404">
        <!--  https://codepen.io/sarazond/pen/jOKyjZ  -->
        <section style="height: 100vh;">
            <svg width="380px" height="500px" viewBox="0 0 837 1045" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                    <path d="M353,9 L626.664028,170 L626.664028,487 L353,642 L79.3359724,487 L79.3359724,170 L353,9 Z" id="Polygon-1" stroke="#007FB2" stroke-width="6" sketch:type="MSShapeGroup"></path>
                    <path d="M78.5,529 L147,569.186414 L147,648.311216 L78.5,687 L10,648.311216 L10,569.186414 L78.5,529 Z" id="Polygon-2" stroke="#EF4A5B" stroke-width="6" sketch:type="MSShapeGroup"></path>
                    <path d="M773,186 L827,217.538705 L827,279.636651 L773,310 L719,279.636651 L719,217.538705 L773,186 Z" id="Polygon-3" stroke="#795D9C" stroke-width="6" sketch:type="MSShapeGroup"></path>
                    <path d="M639,529 L773,607.846761 L773,763.091627 L639,839 L505,763.091627 L505,607.846761 L639,529 Z" id="Polygon-4" stroke="#F2773F" stroke-width="6" sketch:type="MSShapeGroup"></path>
                    <path d="M281,801 L383,861.025276 L383,979.21169 L281,1037 L179,979.21169 L179,861.025276 L281,801 Z" id="Polygon-5" stroke="#36B455" stroke-width="6" sketch:type="MSShapeGroup"></path>
                </g>
            </svg>
            <div class="message-box">
                <a class="logo" href="/">
                    <img src="/images/logo/logo-1.png" alt="logo" class="logo">
                </a>
                <h1>404</h1>
                <p>Không tìm thấy trang hoặc liên kết không đúng</p>
                <div class="buttons-con">
                    <div class="action-link-wrap">
                        <a onclick="history.back(-1)" class="link-button link-back-button">Quay lại</a>
                        <a href="/" class="link-button">Đi đến Trang chủ</a>
                    </div>
                </div>
            </div>
        </section>
    </div>
</body>
</html>
<script src="/js/script.js"></script>
<style>
    /* https://codepen.io/sarazond/pen/jOKyjZ  */
    body {
        /* background-color: #2F3242; */
        background: #fff;
    }
    #page-404 section {
        display: flex;
        align-items: center;
        justify-content: center;
        padding: 15px;
    }

    .message-box {
        width: 100%;
        max-width: 450px;
        padding: 0 15px;
        margin-left: 40px;
        color: var(--text-color-body);
    }

    .message-box .logo {
        height: 80px;
        display: inline-block;
    }

    .message-box h1 {
        font-size: 80px;
        margin-bottom: 40px;
        font-weight: 800;
    }

    .message-box p {
        font-size: 20px;
        font-weight: 300;
    }

    .buttons-con .action-link-wrap {
        margin-top: 40px;
        display: flex;
    }

    .buttons-con .action-link-wrap a {
        box-shadow: -1px 1px 6px 0 rgb(0 0 0 / 9%);
        color: var(--text-color-body);
        background: #fff;
        overflow: hidden;
        font-size: 13px;
        border-radius: 20px;
        padding: 0px 20px;
        font-weight: 500;
        letter-spacing: .2px;
        display: flex;
        align-items: center;
        justify-content: center;
        height: 38px;
        transition: all 0.3s linear;
        cursor: pointer;
        text-decoration: none;
        margin-right: 10px
    }

    .buttons-con .action-link-wrap a:hover {
        /* background: #5A5C6C; */
        color: var(--text-hover);
    }

    g#Page-1>path {
        stroke: var(--text-hover);
    }

    #Polygon-1,
    #Polygon-2,
    #Polygon-3,
    #Polygon-4,
    #Polygon-4,
    #Polygon-5 {
        animation: float 1s infinite ease-in-out alternate;
    }

    #Polygon-2 {
        animation-delay: .2s;
    }

    #Polygon-3 {
        animation-delay: .4s;
    }

    #Polygon-4 {
        animation-delay: .6s;
    }

    #Polygon-5 {
        animation-delay: .8s;
    }

    @keyframes float {
        100% {
            transform: translateY(20px);
        }
    }

    @media (max-width: 768px) {
        #page-404 section{
            flex-direction: column;
        }
        .message-box {
            text-align: center;
            margin-left: 0;
            margin-top: 40px;
        }
        .buttons-con .action-link-wrap{
            justify-content: center;
        }
        svg {
            width: 50%;
            height: auto;
        }
    }
    @media (max-width: 930px) and (orientation: landscape){
        svg {
            width: 25%;
            height: auto;
        }
        .message-box h1 {
            margin-bottom: 20px;
        }
        .buttons-con .action-link-wrap{
            margin-top: 20px
        }
    }
</style>