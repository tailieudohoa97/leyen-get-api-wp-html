<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include("./head.html") ?>
    </head>
    <?php include("./token-headers.php") ?>

    <body class="print-page">
        <!-- Header  -->
        <?php include("./header.php")?>
        <!-- End Header  --> 

        <div id="binh-page" class="scroll-overlay">
            <!-- INTRO PAGE -->
            <section class="intro-page">
                <div class="page-content">
                    <div class="intro-page row">
                        <div class="col-12 col-lg-3 name-page">Print</div>
                        <div class="col-12 col-lg-9 cont-intro">
                            <p><em>Emergence</em> publishes an annual print edition featuring a collection of essays, interviews, poems, adapted multimedia stories, and photo essays. Tactile and intimate, Volumes 1–3 of our print edition span several hundred color-filled pages and a variety of textures of paper, inviting you to slow down and enjoy these stories over time. And for the first time we’re offering a companion soundtrack with our latest volume, available as a limited-edition vinyl LP. We also print smaller collections of work, including practice booklets and our inaugural fiction collection, <em>Short Stories of Apocalypse</em>.</p>
                        </div>
                    </div>
                    <div class="print-page__store-buttons">
                        <a class="" href="#" target="_blank">US & Canada Store</a>
                        <a class="" href="#" target="_blank">International Store</a>
                    </div>
                </div>
            </section>
            <!-- End - INTRO PAGE -->

            <!-- SHOW BLOG - GRID -->
            <section class="show-blog-grid">
                <div class="page-content">
                    <div class="blog-grid">
                        <a href="#" class="blog-item">
                            <div class="blog-item--des">
                                <div class="blog-item--name">Giới thiệu bài viết 1</div>
                                <div class="blog-item--short-description">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown pri nter took a galley of type and scrambled it to make a type specimen book.</p>
                                </div>
                            </div>
                            <div class="blog-item--img">
                                <img src="https://emergencemagazine.org/app/uploads/2022/02/001_LP_WR-768x560.jpg" alt="">
                            </div>
                        </a>
                        <a href="#" class="blog-item">
                            <div class="blog-item--des">
                                <div class="blog-item--name">Giới thiệu bài viết 2</div>
                                <div class="blog-item--short-description">
                                    <p>Sài Gòn tôi yêu đã miêu tả thật chân thực, sinh động hình ảnh thiên nhiên tươi đẹp và nhịp sống Sài Gòn sôi động, ồn ã. Những sự vật luôn luôn có chiều hướng thay đổi trạng thái theo thời gian. Bằng nghệ thuật liệt kê hàng loạt những đặc điểm về thiên nhiên và nhịp sống Sài thành kết hợp với điệp ngữ "yêu", tác giả đã thể hiện được tình yêu, lòng say mê và trân trọng đối với mảnh  đất quê hương mình. Đó là thứ tình cảm thiết tha, mặn nồng. Tac giả yêu nắng, yêu mưa, yêu "thời tiết trái chứng với trời đang ui</p>
                                </div>
                            </div>
                            <div class="blog-item--img">
                                <img src="https://emergencemagazine.org/app/uploads/2021/03/Vol-2-Header-B-768x480.jpg" alt="">
                            </div>
                        </a>
                        <a href="#" class="blog-item">
                            <div class="blog-item--des">
                                <div class="blog-item--name">Giới thiệu bài viết 3</div>
                                <div class="blog-item--short-description">
                                    <p>Oscar-nominated composer Volker Bertelmann (also known as Hauschka) created a companion soundtrack for the third volume of our print edition, <em>Living with the Unknown</em>, a collection of stories that asks: What does living in an unfolding apocalyptic reality look like? His visceral score offers a potent space to encounter the questions and ideas posed in this volume. Each track corresponds to one of the themes: Initiation, Ashes, Roots, and Futures.</p>
                                    <p>The limited-edition 180-gram vinyl features artwork by internationally acclaimed artist Ann Hamilton and a 24-page concordance produced in collaboration with her studio.</p>
                                </div>
                            </div>
                            <div class="blog-item--img">
                                <img src="https://emergencemagazine.org/app/uploads/2021/03/Headers-Desktop-Final-768x480.jpg" alt="">
                            </div>
                        </a>
                    </div>
                </div>
            </section>
            <!-- END - SHOW BLOG - GRID -->

            <!-- OTHER WEBSITE -->
            <section class="other-website">
                <div class="page-content">
                    <div class="stockists row">
                        <div class="col-12 col-lg-2 title-sec"> 
                            <p>Stockists</p>
                        </div>
                        <div class="col-12 col-lg-10 content-sec">
                            <div id="accordion-print" class="accordion-default">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                              Collapsible Group Item #1
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion-print">
                                        <div class="card-body">
                                            <div class="col-6 ml-auto">
                                                <a href="#">
                                                    <div class="name">Daily Edition</div>
                                                    <div class="location">Birmingham, AL</div>
                                                </a>
                                                <a href="#">
                                                    <div class="name">Newsbeat</div>
                                                    <div class="location">Davis, CA</div>
                                                </a>
                                                <a href="#">
                                                    <div class="name">Skylight Books</div>
                                                    <div class="location">Los Angeles, CA</div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                              Collapsible Group Item #2
                                          </button>
                                        </h5>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion-print">
                                        <div class="card-body">
                                            <div class="col-6 ml-auto">
                                                <a href="#">
                                                    <div class="name">Daily Edition</div>
                                                    <div class="location">Birmingham, AL</div>
                                                </a>
                                                <a href="#">
                                                    <div class="name">Newsbeat</div>
                                                    <div class="location">Davis, CA</div>
                                                </a>
                                                <a href="#">
                                                    <div class="name">Skylight Books</div>
                                                    <div class="location">Los Angeles, CA</div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingThree">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                Collapsible Group Item #3
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion-print">
                                        <div class="card-body">
                                            <div class="col-6 ml-auto">
                                                <a href="#">
                                                    <div class="name">Daily Edition</div>
                                                    <div class="location">Birmingham, AL</div>
                                                </a>
                                                <a href="#">
                                                    <div class="name">Newsbeat</div>
                                                    <div class="location">Davis, CA</div>
                                                </a>
                                                <a href="#">
                                                    <div class="name">Skylight Books</div>
                                                    <div class="location">Los Angeles, CA</div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END - OTHER WEBSITE -->

            <!-- Footer  -->
            <?php include("./footer.html") ?>            
            <!-- End Footer  -->
        </div> 
    </body>
</html>
<script src="/js/script.js"></script>
<link rel="stylesheet" href="/styles/print.css">