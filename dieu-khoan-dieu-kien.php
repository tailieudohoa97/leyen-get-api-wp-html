<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include("./head.html") ?>      
    </head>
    
    <?php 
        //Lấy thông tin của slug trang Điều khoản
        $data = getAPI("https://leyen.life/wp-json/wp/v2/pages/1533707?_fields=id,title,content,_links.author,_links.wp:featuredmedia,_embedded&_embed=wp:featuredmedia,author"); 

        if($data == '[]'){
            header('HTTP/1.1 404 Not Found');
            // Chuyển link thành ...404/ luôn thì dùng dòng dưới 
            // header('Location: '.'http://' . $_SERVER['HTTP_HOST'].'/404/') 
            include '404.php';
            return;
        }
    ?>

<body class="about-page dieu-khoan-dieu-kien">
    <!-- Header  -->
    <?php include("./header.php")?>
    <!-- End Header  --> 
    
    <div id="about-page" class="scroll-overlay">

        <!-- INTRO PAGE -->
        <section class="intro-page">
            <div class="page-content">
                <div class="intro-page row">
                    <div class="col-12 col-lg-5 col-xxl-7 name-page">
                        <p>{{ aboutPage.title.rendered }}</p>
                        <!-- <img v-if="aboutPage['_embedded']['wp:featuredmedia'][0]['source_url']" :src="aboutPage['_embedded']['wp:featuredmedia'][0]['source_url']" :alt="aboutPage._embedded['author'][0].name" width="50%"> -->
                    </div>
                    <div class="col-12 col-lg-7 col-xxl-5 cont-intro" v-html="replaceText(aboutPage.content.rendered)">
                    </div>
                </div>
            </div>
        </section>
        <!-- End - INTRO PAGE -->

        <!-- Footer  -->
        <?php include("./footer.html") ?>
        <!-- End Footer  -->
    </div>
</body>
</html>
<script src="/js/script.js"></script>
<script> 
    const loadData_GioiThieu = '<?php echo addslashes($data); ?>';

    const vueData_GioiThieu = JSON.parse(loadData_GioiThieu);

    // console.log("Thông tin trang Giới thiệu:")
    // console.log(vueData_GioiThieu);
</script>
<script src="/js/gioi-thieu-script.js"></script>
<link rel="stylesheet" href="/styles/gioi-thieu.css">